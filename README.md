# Counting multilayer temporal motifs

The **snap-adv/** folder contains code which can be included in the Stanford Network Analysis Platform ([SNAP](http://snap.stanford.edu/)) in order to be able to **count multilayer temporal motifs**.  An example usage of the code is included in the **examples/multilayertemporalmotifs/** folder.

To be able to run the code and example they should be included in SNAP, available at <https://github.com/snap-stanford/snap>, in the folders by the same name.

Last check for compatibility with SNAP has been made on June 4th 2020.

## Code versions

The current version of code is produced for the paper entitled: [Investigating scientific mobility in co-authorship networks using multilayer temporal motifs](https://doi.org/10.1017/nws.2021.12)

This code also implements the approaches presented in the following publications:

* [Efficiently counting complex multilayer temporal motifs in large-scale networks](https://link.springer.com/article/10.1186/s40649-019-0068-z)
* [Counting Multilayer Temporal Motifs in Complex Networks](https://link.springer.com/chapter/10.1007/978-3-030-05411-3_46)
(pseudocode from this publication is available in the **psuedocode/** folder)

## Datasets
The **data/** folder contains the five co-authorship networks obtained from [Web of Science](https://clarivate.com/webofsciencegroup/solutions/web-of-science/) which were used in the publication entitled: Investigating scientific mobility in co-authorship networks using multilayer temporal motifs (currently under consideration).
Included in the **data/** folder is also the preprocessing script used by the aforementioned work to generate the datasets which were input in the multilayer temporal motif counting code.

Each dataset captures the co-authorship between scholars in specific scientific fields in the period 2007--2016. The datasets contain the following columns:

 - auid_one: first co-author
 - auid_two: second co-author
 - pub_year: publication year
 - coll_type: collaboration type
     0. Organizational co-authorship;
     1. Local co-authorship (same city);
     2. National co-authorship;
     3. International co-authorship
 - pid: publication on which this co-authorship occurred
 - country_one: country to which the first co-author is affiliated through their organization
 - country_two: country to which the second co-author is affiliated through their organization

Note that although authors and publications may be active in multiple scientific fields, they (often) have different auid's and pid's between the various datasets. Furthermore, when country information is missing *NAN* is specified as the country, thus *NAN* does not refer to an actual country.

For further details on the datasets and their extraction we refer you to the above specified publication.

