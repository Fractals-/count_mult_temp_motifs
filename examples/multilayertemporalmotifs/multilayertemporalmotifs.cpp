// multilayertemporalmotifsmain.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <dirent.h>

#include "multilayertemporalmotifs.h"
#ifdef USE_OPENMP
#include <omp.h>
#endif

struct timespec start, finish, startt, finishh;
double elapsed, elapsedd;

void writeDirectedResults(FILE* output_file, Counter2D& counts,
                          Counter3D& starcounts, Counter3D& trianglecounts){
  fprintf(output_file, "3-edge edge motifs:\n");
  for (int k = 0; k < counts.n(); k++) {
    fprintf(output_file, "Layer permutation %d:\n", k+1);
    for (int i = 0; i < 4; i++) {
      fprintf(output_file, "%ld %ld\n", counts(i, k).Val, counts(i+4, k).Val);
    }
    for (int i = 8; i < 12; i++) {
      fprintf(output_file, "x %ld\n", counts(i, k).Val);
    }
    fprintf(output_file, "\n");
  }

  fprintf(output_file, "========================\n3-node, 3-edge star motifs:\n");
  for (int i = 0; i < starcounts.p(); i++){
    fprintf(output_file, "Layer permutation %d:\n", i+1);
    for (int j = 0; j < starcounts.m(); j++){
      for (int k = 0; k < starcounts.n(); k++){
        if ( (j == 0 || j == 2 || j == 4) && (k == 2 || k == 6)) {
          fprintf(output_file, "x");
        } else
          fprintf(output_file, "%ld", starcounts(j, k, i).Val);
        if ( k < starcounts.n() - 1) { fprintf(output_file, " "); }
      }
      fprintf(output_file, "\n");
    }
    fprintf(output_file, "\n");
  }

  fprintf(output_file, "========================\n3-node, 3-edge triangle motifs:\n");
  for (int i = 0; i < trianglecounts.p(); i++){
    fprintf(output_file, "Layer permutation %d:\n", i+1);
    for (int j = 0; j < trianglecounts.m(); j++){
      for (int k = 0; k < trianglecounts.n(); k++){
        if ( (j == 0 && k > 1) || (j == 1 && (k == 3 || k > 4)) || (j == 2 && k > 3)) {
          fprintf(output_file, "x");
        } else
          fprintf(output_file, "%ld", trianglecounts(j, k, i).Val);
        if ( k < trianglecounts.n() - 1) { fprintf(output_file, " "); }
      }
      fprintf(output_file, "\n");
    }
  }
}

void writeUndirectedResults(FILE* output_file, Counter2D& counts,
                            Counter3D& starcounts, Counter3D& trianglecounts){
  fprintf(output_file, "3-edge edge motifs:\n");
  for (int k = 0; k < counts.n(); k++) {
    fprintf(output_file, "Layer permutation %d: ", k+1);
    TInt64 count_1 = counts(0, k) + counts(4, k);
    TInt64 count_2 = counts(1, k) + counts(5, k) + counts(8, k);
    TInt64 count_3 = counts(2, k) + counts(6, k) + counts(9, k);
    TInt64 count_4 = counts(3, k) + counts(7, k) + counts(10, k) + counts(11, k);
    fprintf(output_file, "%ld %ld %ld %ld\n", count_1.Val, count_2.Val, count_3.Val, count_4.Val);
  }
  fprintf(output_file, "\n");

  fprintf(output_file, "========================\n3-node, 3-edge star motifs:\n");
  for (int i = 0; i < starcounts.p(); i++){
    fprintf(output_file, "Layer permutation %d: ", i+1);
    for (int j = 0; j < starcounts.m(); j++){
      TInt64 count = 0;
      for (int k = 0; k < starcounts.n(); k++){
        if ( !((j == 0 || j == 2 || j == 4) && (k == 2 || k == 6)))
          count += starcounts(j, k, i);
      }
      fprintf(output_file, "%ld", count.Val);
      if ( j < starcounts.m() - 1) { fprintf(output_file, " "); }
    }
    fprintf(output_file, "\n");
  }
  fprintf(output_file, "\n");

  fprintf(output_file, "========================\n3-node, 3-edge triangle motifs:\n");
  for (int i = 0; i < trianglecounts.p(); i++){
    fprintf(output_file, "Layer permutation %d: ", i+1);
    for (int j = 0; j < trianglecounts.m(); j++){
      TInt64 count = 0;
      for (int k = 0; k < trianglecounts.n(); k++){
        if ( !((j == 0 && k > 1) || (j == 1 && (k == 3 || k > 4)) || (j == 2 && k > 3)))
          count += trianglecounts(j, k, i);
      }
      fprintf(output_file, "%ld", count.Val);
      if ( j < trianglecounts.m() - 1) { fprintf(output_file, " "); }
    }
    fprintf(output_file, "\n");
  }
}

void multilayerTemporalCounting(const TStr graph_filename, const TStr output_filename, const TFlt delta,
                                  const bool ml_flag, const bool eae_flag, const bool undir_flag){
  fprintf(stderr, "Processing %s\n", graph_filename.CStr());
  // Count all 2-node and 3-node temporal motifs with 3 temporal edges
  MultTempMotifCounter mtmc(graph_filename, ml_flag, eae_flag);
  fprintf(stderr, " Finished reading in\n");
  clock_gettime(CLOCK_MONOTONIC, &startt);
  Counter2D counts;
  Counter3D starcounts, trianglecounts;
  mtmc.Count3MTEdge23Node(delta, counts, starcounts, trianglecounts, undir_flag);
  clock_gettime(CLOCK_MONOTONIC, &finishh);
  elapsedd = (finishh.tv_sec - startt.tv_sec);
  elapsedd += (finishh.tv_nsec - startt.tv_nsec) / 1000000000.0;
  fprintf(stderr, " Finished counting in %f (%s)\n", elapsedd, TSecTm::GetCurTm().GetTmStr().CStr());

  // Write results to output file
  FILE* output_file = fopen(output_filename.CStr(), "wt");
  fprintf(output_file, "Motif counting results for: %s\n", graph_filename.CStr());
  fprintf(output_file, "Delta was set to: %.1f\n", double(delta));
  fprintf(output_file, "Motif counting runtime: %fs\n\n", elapsedd);
  if (undir_flag){
    writeUndirectedResults(output_file, counts, starcounts, trianglecounts);
  } else {
    writeDirectedResults(output_file, counts, starcounts, trianglecounts);
  }
  fprintf(stderr, " Results written to output file\n");
}

int main(int argc, char* argv[]) {
  Env = TEnv(argc, argv, TNotify::StdNotify);
  Env.PrepArgs(TStr::Fmt("MultilayerTemporalmotifs. build: %s, %s. Time: %s",
			 __TIME__, __DATE__, TExeTm::GetCurTm()));
  clock_gettime(CLOCK_MONOTONIC, &start);
  Try

  const TStr graph_filename =
    Env.GetIfArgPrefixStr("-i:", "example-multilayer-temporal-graph.txt", "Input file");
  const TStr output_filename =
    Env.GetIfArgPrefixStr("-o:", "example-output.txt", "Output file");
  const TFlt delta =
    Env.GetIfArgPrefixFlt("-delta:", 10, "Time window delta");
  const int num_threads =
    Env.GetIfArgPrefixInt("-nt:", 1, "Number of threads for parallelization");
  const bool ml_flag =
    Env.GetIfArgPrefixBool("-ml:", true, "Multilayer counting (T), single-layer counting (F)");
  const bool eae_flag =
    Env.GetIfArgPrefixBool("-eae:", true, "Enforce edge attribute exclusivity (T or F)");
  const bool undir_flag =
    Env.GetIfArgPrefixBool("-undir:", false, "Undirected (T) or directed (F) results");

#ifdef USE_OPENMP
  omp_set_num_threads(num_threads);
#endif

  multilayerTemporalCounting(graph_filename, output_filename, delta, ml_flag, eae_flag, undir_flag);

  Catch
  clock_gettime(CLOCK_MONOTONIC, &finish);
  elapsed = (finish.tv_sec - start.tv_sec);
  elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
  fprintf(stderr, "\nTotal runtime: %f (%s)\n", elapsed, TSecTm::GetCurTm().GetTmStr().CStr());
  return 0;
}
