import sys
import os
from operator import itemgetter

countries = set()

def preprocess(infile, outfile):
  authors = {}
  with open(outfile, 'w') as out:
    with open(infile, 'r') as inf:
      auid = 1
      i = 0
      for line in inf:
        if i == 0:
          i += 1
          continue
        elems = line.split('\t')
        # Reassign author ID's (so no ID's are skipped)
        if elems[0] not in authors:
          authors[elems[0]] = auid
          auid += 1
        if elems[1] not in authors:
          authors[elems[1]] = auid
          auid += 1
        # Output the preprocessed file with reassigned author ID's
        out.write(str(authors[elems[0]]) + '\t' + str(authors[elems[1]]) + '\t' + elems[2] + '\t' + elems[3] + '\t' + elems[4] + "\n")

def determineCountries(infile):
  with open(infile, 'r') as inf:
    i = 0
    for line in inf:
      if i == 0:
        i += 1
        continue
      elems = line.split('\t')
      countries.update([elems[5],elems[6][:-1]])

def determineAssociatedCountryPubs(infile):
  papers = {country: set() for country in countries}
  # Determine papers associated with each country
  with open(infile, 'r') as inf:
    i = 0
    for line in inf:
      if i == 0:
        i += 1
        continue
      elems = line.split('\t')
      if elems[5] != 'NAN':
        papers[elems[5]].add(elems[4])
      if elems[6][:-1] != 'NAN':
        papers[elems[6][:-1]].add(elems[4])
  return papers

def preprocessCountry(infile, outfile, country, papers):
  authors = {}
  with open(outfile, 'w') as out:
    with open(infile, 'r') as inf:
      auid = 1
      i = 0
      for line in inf:
        if i == 0:
          i += 1
          continue
        elems = line.split('\t')
        if elems[4] in papers:
          # Reassign author ID's (so no ID's are skipped)
          if elems[0] not in authors:
            authors[elems[0]] = auid
            auid += 1
          if elems[1] not in authors:
            authors[elems[1]] = auid
            auid += 1
          # Output for this country all collaborations associated with these papers
          out.write(str(authors[elems[0]]) + '\t' + str(authors[elems[1]]) + '\t' + elems[2] + '\t' + elems[3] + '\t' + elems[4] + "\n")

if __name__ == "__main__":
  if len(sys.argv) < 3:
    print("Error - expected input format: preprocess.py <input_filename> <output_folder>")
    sys.exit()
  infile = sys.argv[1]
  if not os.path.isfile(infile):
    print("Error - inputfile does not exist")
    sys.exit()
  outfolder = sys.argv[2]
  os.makedirs(os.path.join(os.getcwd(), outfolder), exist_ok=True)

  # Create full preprocessed file
  print("Generating world dataset...", end='\r')
  preprocess(infile, outfolder + "/000-WORLD.dat")
  print("Generating world dataset... DONE")

  # Determine countries in dataset
  print("Determining countries in dataset...", end='\r')
  determineCountries(infile)
  countries.remove("NAN") # Remove NULL country
  print("Determining countries in dataset... DONE")

  # Determine papers associated with each country
  print("Determining associated publications for each country...", end='\r')
  papers = determineAssociatedCountryPubs(infile)
  print("Determining associated publications for each country... DONE")

  # Order the countries by number of associated papers
  print("Ordering countries by number of associated publications...", end='\r')
  order = [(len(papers[country]), country)  for country in countries]
  order.sort(key=itemgetter(0), reverse=True)
  print("Ordering countries by number of associated publications... DONE")

  # Create a preprocessed dataset file per country
  print("Generating datasets for each of the ", len(countries), " countries:")
  if outfolder[-1] != '/':
    outfolder = outfolder + '/'
  i = 1
  for item in order:
    if i < 10:
      targetfile = outfolder + "00" + str(i) + "-" + item[1] + ".dat"
    elif i < 100:
      targetfile = outfolder + "0" + str(i) + "-" + item[1] + ".dat"
    else:
      targetfile = outfolder + str(i) + "-" + item[1] + ".dat"
    i += 1
    print("Generating ", targetfile, end='\r')
    preprocessCountry(infile, targetfile, item[1], papers[item[1]])
    print("Generating ", targetfile, " DONE")