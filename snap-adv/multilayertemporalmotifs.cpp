#include "Snap.h"
#include "multilayertemporalmotifs.h"

//==================================================================================
// Initialization and helper methods for MultTempMotifCounter
MultTempMotifCounter::MultTempMotifCounter(const TStr& filename, bool ml, bool eae) {
  // First load the static graph
  static_graph_ = TSnap::LoadEdgeList<PNGraph>(filename, 0, 1);
  int max_nodes = static_graph_->GetMxNId();
  temporal_data_ = TVec< THash<TInt, TIntV> >(max_nodes);
  layer_data_ = TVec< THash<TInt, TIntV> >(max_nodes);
  edge_attribute_data_ = TVec< THash<TInt, TIntV> >(max_nodes);
  int max_layer = 0;

  // Formulate input File Format:
  //   source_node destination_node timestamp layer (edge_attribute)
  TTableContext context;
  Schema temp_graph_schema;
  temp_graph_schema.Add(TPair<TStr,TAttrType>("source", atInt));
  temp_graph_schema.Add(TPair<TStr,TAttrType>("destination", atInt));
  temp_graph_schema.Add(TPair<TStr,TAttrType>("time", atInt));
  temp_graph_schema.Add(TPair<TStr,TAttrType>("layer", atInt));
  temp_graph_schema.Add(TPair<TStr,TAttrType>("edge_attribute", atInt));

  // Load the multilayer temporal graph
  PTable data_ptr = TTable::LoadSS(temp_graph_schema, filename, &context, '\t');
  TInt src_idx = data_ptr->GetColIdx("source");
  TInt dst_idx = data_ptr->GetColIdx("destination");
  TInt tim_idx = data_ptr->GetColIdx("time");
  TInt lay_idx = data_ptr->GetColIdx("layer");
  TInt pid_idx = data_ptr->GetColIdx("edge_attribute");
  for (TRowIterator RI = data_ptr->BegRI(); RI < data_ptr->EndRI(); RI++) {
    TInt row_idx = RI.GetRowIdx();
    int src = data_ptr->GetIntValAtRowIdx(src_idx, row_idx).Val;
    int dst = data_ptr->GetIntValAtRowIdx(dst_idx, row_idx).Val;
    int tim = data_ptr->GetIntValAtRowIdx(tim_idx, row_idx).Val;
    int lay = data_ptr->GetIntValAtRowIdx(lay_idx, row_idx).Val;
    int pid = data_ptr->GetIntValAtRowIdx(pid_idx, row_idx).Val;
    if (!ml) { lay = 0; }
    if (!eae) { pid =  row_idx; }
    if (lay > max_layer) { max_layer = lay; }

    // Do not include self loops as they do not appear in the definition of
    // temporal motifs.
    if (src != dst) {
      temporal_data_[src](dst).Add(tim);
      layer_data_[src](dst).Add(lay);
      edge_attribute_data_[src](dst).Add(pid);
    }
  }

  nrlayers_ = max_layer + 1;
}

void MultTempMotifCounter::GetAllNodes(TIntV& nodes) {
  nodes = TIntV();
  for (TNGraph::TNodeI it = static_graph_->BegNI();
       it < static_graph_->EndNI(); it++) {
    nodes.Add(it.GetId());
  }
}

bool MultTempMotifCounter::HasEdges(int u, int v) {
  return temporal_data_[u].IsKey(v);
}

void MultTempMotifCounter::GetAllNeighbors(int node, TIntV& nbrs) {
  nbrs = TIntV();
  TNGraph::TNodeI NI = static_graph_->GetNI(node);
  for (int i = 0; i < NI.GetOutDeg(); i++) { nbrs.Add(NI.GetOutNId(i)); }
  for (int i = 0; i < NI.GetInDeg(); i++) {
    int nbr = NI.GetInNId(i);
    if (!NI.IsOutNId(nbr)) { nbrs.Add(nbr); }
  }
}

void MultTempMotifCounter::GetAllStaticTriangles(TIntV& Us, TIntV& Vs, TIntV& Ws) {
  Us.Clr();
  Vs.Clr();
  Ws.Clr();
  // Get degree ordering of the graph
  int max_nodes = static_graph_->GetMxNId();
  TVec<TIntPair> degrees(max_nodes);
  degrees.PutAll(TIntPair(0, 0));
  // Set the degree of a node to be the number of nodes adjacent to the node in
  // the undirected graph.
  TIntV nodes;
  GetAllNodes(nodes);
  #pragma omp parallel for schedule(dynamic)
  for (int node_id = 0; node_id < nodes.Len(); node_id++) {
    int src = nodes[node_id];
    TIntV nbrs;
    GetAllNeighbors(src, nbrs);
    degrees[src] = TIntPair(nbrs.Len(), src);
  }
  degrees.Sort();
  TIntV order = TIntV(max_nodes);
  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < order.Len(); i++) {
    order[degrees[i].Dat] = i;
  }

  // Get triangles centered at a given node where that node is the smallest in
  // the degree ordering.
  #pragma omp parallel for schedule(dynamic)
  for (int node_id = 0; node_id < nodes.Len(); node_id++) {
    int src = nodes[node_id];
    int src_pos = order[src];

    // Get all neighbors who come later in the ordering
    TIntV nbrs;
    GetAllNeighbors(src, nbrs);
    TIntV neighbors_higher;
    for (int i = 0; i < nbrs.Len(); i++) {
      int nbr = nbrs[i];
      if (order[nbr] > src_pos) { neighbors_higher.Add(nbr); }
    }

    for (int ind1 = 0; ind1 < neighbors_higher.Len(); ind1++) {
      for (int ind2 = ind1 + 1; ind2 < neighbors_higher.Len(); ind2++) {
        int dst1 = neighbors_higher[ind1];
        int dst2 = neighbors_higher[ind2];
        // Check for triangle formation
        if (static_graph_->IsEdge(dst1, dst2) || static_graph_->IsEdge(dst2, dst1)) {
          #pragma omp critical
          {
            Us.Add(src);
            Vs.Add(dst1);
            Ws.Add(dst2);
          }
        }
      }
    }
  }
}

void MultTempMotifCounter::Count3MTEdge23Node(double delta, Counter2D& counts, Counter3D& starcounts,
                                              Counter3D& trianglecounts, bool undir ) {
  // This is simply a wrapper function around the counting methods to produce
  // counts in the same way that they were represented in the paper.  This makes
  // it easy to reproduce results and allow SNAP users to make the same
  // measurements on their multilayer temporal network data.
  int total_motifs = (nrlayers_ * nrlayers_ * nrlayers_), a, b, c, d, e, f;

  // 2 node motifs
  Counter7D edge_counts;
  Count3MTEdge2Node(delta, edge_counts);
  counts = Counter2D(12, total_motifs);
  for (int i = 0; i < total_motifs; i++) {
    c = (i / (nrlayers_ * nrlayers_));
    b = (i / nrlayers_) % nrlayers_;
    a = i % nrlayers_;
    // Timing: 111
    counts(0,i) = edge_counts(0, 0, a, 0, b, 0, c) + edge_counts(0, 1, a, 1, b, 1, c);  // M_{e,1,1}
    counts(4,i) = edge_counts(0, 1, a, 0, b, 0, c) + edge_counts(0, 0, a, 1, b, 1, c)
                + edge_counts(0, 0, b, 1, a, 0, c) + edge_counts(0, 1, b, 0, a, 1, c)
                + edge_counts(0, 0, b, 0, c, 1, a) + edge_counts(0, 1, b, 1, c, 0, a);  // M_{e,1,2}
    // Timing: 112
    counts(1,i) = edge_counts(1, 0, a, 0, b, 0, c) + edge_counts(1, 1, a, 1, b, 1, c);  // M_{e,2,1}
    counts(5,i) = edge_counts(1, 1, a, 0, b, 0, c) + edge_counts(1, 0, a, 1, b, 1, c)
                + edge_counts(1, 0, b, 1, a, 0, c) + edge_counts(1, 1, b, 0, a, 1, c);  // M_{e,2,2}
    counts(8,i) = edge_counts(1, 0, a, 0, b, 1, c) + edge_counts(1, 1, a, 1, b, 0, c);  // M_{e,5,2}
    // Timing: 122
    counts(2,i) = edge_counts(2, 0, a, 0, b, 0, c) + edge_counts(2, 1, a, 1, b, 1, c);  // M_{e,3,1}
    counts(6,i) = edge_counts(2, 1, a, 0, b, 0, c) + edge_counts(2, 0, a, 1, b, 1, c);  // M_{e,3,2}
    counts(9,i) = edge_counts(2, 0, a, 1, b, 0, c) + edge_counts(2, 1, a, 0, b, 1, c)
                + edge_counts(2, 0, a, 0, c, 1, b) + edge_counts(2, 1, a, 1, c, 0, b);  // M_{e,6,2}
    // Timing: 123
    counts(3,i)  = edge_counts(3, 0, a, 0, b, 0, c) + edge_counts(3, 1, a, 1, b, 1, c); // M_{e,4,1}
    counts(7,i)  = edge_counts(3, 1, a, 0, b, 0, c) + edge_counts(3, 0, a, 1, b, 1, c); // M_{e,4,2}
    counts(10,i) = edge_counts(3, 0, a, 1, b, 0, c) + edge_counts(3, 1, a, 0, b, 1, c); // M_{e,7,2}
    counts(11,i) = edge_counts(3, 0, a, 0, b, 1, c) + edge_counts(3, 1, a, 1, b, 0, c); // M_{e,8,2}
  }
  // Combine equivalent layer configurations on parallel concurrent edges
  for (int i = 0; i < nrlayers_; i++){
    for (int j = i; j < nrlayers_; j++){
      for (int k = j; k < nrlayers_; k++){
        // Timing 111
        if (i == j && i == k) continue; // 000, 111 etc., no equivalences
        int lc_111_switch_a = i + j * nrlayers_ + k * (nrlayers_ * nrlayers_);
        int lc_111_switch_b = i + k * nrlayers_ + j * (nrlayers_ * nrlayers_);
        int lc_111_switch_c = j + i * nrlayers_ + k * (nrlayers_ * nrlayers_);
        int lc_111_switch_d = j + k * nrlayers_ + i * (nrlayers_ * nrlayers_);
        int lc_111_switch_e = k + i * nrlayers_ + j * (nrlayers_ * nrlayers_);
        int lc_111_switch_f = k + j * nrlayers_ + i * (nrlayers_ * nrlayers_);
        for (int m = 0; m < 5; m += 4) {
          if (undir || m == 0) { // when directed results do this only for m == 0
            if (i == j){ // 001, 002, 112 etc., combine 001, 010, 100
              counts(m, lc_111_switch_a) += counts(m, lc_111_switch_b) + counts(m, lc_111_switch_e);
              counts(m, lc_111_switch_b)  = counts(m, lc_111_switch_a);
              counts(m, lc_111_switch_e)  = counts(m, lc_111_switch_a);
            }
            else if (j == k){ //011, 022, 122 etc., combine 011, 101, 110
              counts(m, lc_111_switch_a) += counts(m, lc_111_switch_c) + counts(m, lc_111_switch_d);
              counts(m, lc_111_switch_c)  = counts(m, lc_111_switch_a);
              counts(m, lc_111_switch_d)  = counts(m, lc_111_switch_a);
            }
            else{ // 012, 013, 123 etc.
              counts(m, lc_111_switch_a) += counts(m, lc_111_switch_b) + counts(m, lc_111_switch_c)
                                          + counts(m, lc_111_switch_d) + counts(m, lc_111_switch_e)
                                          + counts(m, lc_111_switch_f);
              counts(m, lc_111_switch_b)  = counts(m, lc_111_switch_a);
              counts(m, lc_111_switch_c)  = counts(m, lc_111_switch_a);
              counts(m, lc_111_switch_d)  = counts(m, lc_111_switch_a);
              counts(m, lc_111_switch_e)  = counts(m, lc_111_switch_a);
              counts(m, lc_111_switch_f)  = counts(m, lc_111_switch_a);
            }
          }
        }
      }
    }
  }
  for (int i = 0; i < nrlayers_; i++){
    for (int j = i + 1; j < nrlayers_; j++){
      for (int k = 0; k < nrlayers_; k++){
        // Timing 112, i.e., for two layers 100 == 010 AND 101 == 011
        int lc_12_switch_a = i + j * nrlayers_ + k * (nrlayers_ * nrlayers_);
        int lc_12_switch_b = j + i * nrlayers_ + k * (nrlayers_ * nrlayers_);
        counts(1 ,lc_12_switch_a) += counts(1, lc_12_switch_b);
        counts(1, lc_12_switch_b)  = counts(1, lc_12_switch_a);
        if (undir) { // When directed results, no equivalence here
          counts(5, lc_12_switch_a) += counts(5, lc_12_switch_b);
          counts(5, lc_12_switch_b)  = counts(5, lc_12_switch_a);
        }
        counts(8, lc_12_switch_a) += counts(8, lc_12_switch_b);
        counts(8, lc_12_switch_b)  = counts(8, lc_12_switch_a);
        // Timing 122, i.e., for two layers 010 == 001 AND 110 == 101
        int lc_23_switch_a = k + i * nrlayers_ + j * (nrlayers_ * nrlayers_);
        int lc_23_switch_b = k + j * nrlayers_ + i * (nrlayers_ * nrlayers_);
        counts(2, lc_23_switch_a) += counts(2, lc_23_switch_b);
        counts(2, lc_23_switch_b)  = counts(2, lc_23_switch_a);
        counts(6, lc_23_switch_a) += counts(6, lc_23_switch_b);
        counts(6, lc_23_switch_b)  = counts(6, lc_23_switch_a);
        if (undir) {
          counts(9, lc_23_switch_a) += counts(9, lc_23_switch_b);
          counts(9, lc_23_switch_b)  = counts(9, lc_23_switch_a);
        } else {
          counts(4, lc_23_switch_a) += counts(4, lc_23_switch_b);
          counts(4, lc_23_switch_b)  = counts(4, lc_23_switch_a);
        }
      }
    }
  }

  // 3 node star motifs
  Counter7D star_counts;
  Count3MTEdge3NodeStars(delta, star_counts);
  starcounts = Counter3D(8, 8, total_motifs);
  for (int i = 0; i < 8; i++) {
    d = (i / 4);
    e = (i / 2) % 2;
    f = i % 2;
    for (int j = 0; j < total_motifs; j++) {
      c = (j / (nrlayers_ * nrlayers_));
      b = (j / nrlayers_) % nrlayers_;
      a = j % nrlayers_;
      // Match edge order+directions to correct motifs as represented in the paper.
      starcounts(0, i, j) = star_counts(0, f, a, e, b, d, c);  // M_{s,1,x}
      starcounts(1, i, j) = star_counts(1, e, a, d, b, f, c);  // M_{s,2,x}
      starcounts(2, i, j) = star_counts(2, d, a, e, b, f, c);  // M_{s,3,x}
      starcounts(3, i, j) = star_counts(3, d, a, e, b, f, c);  // M_{s,4,x}
      starcounts(4, i, j) = star_counts(4, f, a, e, b, d, c);  // M_{s,5,x}
      starcounts(5, i, j) = star_counts(5, e, a, f, b, d, c);  // M_{s,6,x}
      starcounts(6, i, j) = star_counts(6, e, a, d, b, f, c);  // M_{s,7,x}
      starcounts(7, i, j) = star_counts(7, e, a, f, b, d, c);  // M_{s,8,x}
    }
  }
  // Combine equivalent layer configurations on parallel concurrent edges
  for (int i = 0; i < nrlayers_; i++){
    for (int j = i + 1; j < nrlayers_; j++){
      for (int k = 0; k < nrlayers_; k++){
        // Timing 112, i.e., for two layers 100 == 010 AND 101 == 011
        int lc_12_switch_a = i + j * nrlayers_ + k * (nrlayers_ * nrlayers_);
        int lc_12_switch_b = j + i * nrlayers_ + k * (nrlayers_ * nrlayers_);
        for (int m = 0; m < 8; m++) {
          starcounts(0, m, lc_12_switch_a) += starcounts(0, m, lc_12_switch_b);
          starcounts(0, m, lc_12_switch_b)  = starcounts(0, m, lc_12_switch_a);
          starcounts(4, m, lc_12_switch_a) += starcounts(4, m, lc_12_switch_b);
          starcounts(4, m, lc_12_switch_b)  = starcounts(4, m, lc_12_switch_a);
        }
        // Timing 122, i.e., for two layers 010 == 001 AND 110 == 101
        int lc_23_switch_a = k + i * nrlayers_ + j * (nrlayers_ * nrlayers_);
        int lc_23_switch_b = k + j * nrlayers_ + i * (nrlayers_ * nrlayers_);
        for (int m = 0; m < 8; m++) {
          starcounts(2, m, lc_23_switch_a) += starcounts(2, m, lc_23_switch_b);
          starcounts(2, m, lc_23_switch_b)  = starcounts(2, m, lc_23_switch_a);
        }
      }
    }
  }
  // Resolve for identical motifs
  for (int i = 0; i < total_motifs; i++) {
    starcounts(0, 1, i) += starcounts(0, 2, i); // M_{s,1,2} and M_{s,1,3}
    starcounts(0, 2, i)  = starcounts(0, 1, i);
    starcounts(0, 5, i) += starcounts(0, 6, i); // M_{s,1,6} and M_{s,1,7}
    starcounts(0, 6, i)  = starcounts(0, 5, i);

    starcounts(2, 1, i) += starcounts(2, 2, i); // M_{s,3,2} and M_{s,3,3}
    starcounts(2, 2, i)  = starcounts(2, 1, i);
    starcounts(2, 5, i) += starcounts(2, 6, i); // M_{s,3,6} and M_{s,3,7}
    starcounts(2, 6, i)  = starcounts(2, 5, i);

    starcounts(4, 1, i) += starcounts(4, 2, i); // M_{s,5,2} and M_{s,5,3}
    starcounts(4, 2, i)  = starcounts(4, 1, i);
    starcounts(4, 5, i) += starcounts(4, 6, i); // M_{s,5,6} and M_{s,5,7}
    starcounts(4, 6, i)  = starcounts(4, 5, i);
  }

  // 3 node triangle motifs
  Counter5D triad_counts;
  Count3MTEdgeTriads(delta, triad_counts);
  trianglecounts = Counter3D(4, 8, total_motifs);
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 8; j++) {
      for (int k = 0; k < total_motifs; k++) {
        c = (k / (nrlayers_ * nrlayers_));
        b = (k / nrlayers_) % nrlayers_;
        a = k % nrlayers_;
        trianglecounts(i, j, k) = triad_counts(i, j, a, b, c);
      }
    }
  }
  // Combine equivalent layer configurations on parallel concurrent edges
  for (int i = 0; i < nrlayers_; i++){
    for (int j = i; j < nrlayers_; j++){
      for (int k = j; k < nrlayers_; k++){
        // Timing 111
        if (i == j && i == k) continue; // 000, 111 etc., no equivalences
        int lc_111_switch_a = i + j * nrlayers_ + k * (nrlayers_ * nrlayers_);
        int lc_111_switch_b = i + k * nrlayers_ + j * (nrlayers_ * nrlayers_);
        int lc_111_switch_c = j + i * nrlayers_ + k * (nrlayers_ * nrlayers_);
        int lc_111_switch_d = j + k * nrlayers_ + i * (nrlayers_ * nrlayers_);
        int lc_111_switch_e = k + i * nrlayers_ + j * (nrlayers_ * nrlayers_);
        int lc_111_switch_f = k + j * nrlayers_ + i * (nrlayers_ * nrlayers_);
        for (int m = 0; m < 2; m++) {
          if (undir || m == 1) { // when directed results do this only for m == 1
            if (i == j){ // 001, 002, 112 etc., combine 001, 010, 100
              trianglecounts(0, m, lc_111_switch_a) += trianglecounts(0, m, lc_111_switch_b)
                                                     + trianglecounts(0, m, lc_111_switch_e);
              trianglecounts(0, m, lc_111_switch_b)  = trianglecounts(0, m, lc_111_switch_a);
              trianglecounts(0, m, lc_111_switch_e)  = trianglecounts(0, m, lc_111_switch_a);
            }
            else if (j == k){ //011, 022, 122 etc., combine 011, 101, 110
              trianglecounts(0, m, lc_111_switch_a) += trianglecounts(0, m, lc_111_switch_c)
                                                     + trianglecounts(0, m, lc_111_switch_d);
              trianglecounts(0, m, lc_111_switch_c)  = trianglecounts(0, m, lc_111_switch_a);
              trianglecounts(0, m, lc_111_switch_d)  = trianglecounts(0, m, lc_111_switch_a);
            }
            else{ // 012, 013, 123 etc.
              trianglecounts(0, m, lc_111_switch_a) += trianglecounts(0, m, lc_111_switch_b)
                                                     + trianglecounts(0, m, lc_111_switch_c)
                                                     + trianglecounts(0, m, lc_111_switch_d)
                                                     + trianglecounts(0, m, lc_111_switch_e)
                                                     + trianglecounts(0, m, lc_111_switch_f);
              trianglecounts(0, m, lc_111_switch_b)  = trianglecounts(0, m, lc_111_switch_a);
              trianglecounts(0, m, lc_111_switch_c)  = trianglecounts(0, m, lc_111_switch_a);
              trianglecounts(0, m, lc_111_switch_d)  = trianglecounts(0, m, lc_111_switch_a);
              trianglecounts(0, m, lc_111_switch_e)  = trianglecounts(0, m, lc_111_switch_a);
              trianglecounts(0, m, lc_111_switch_f)  = trianglecounts(0, m, lc_111_switch_a);
            }
          }
        }
      }
    }
  }
  if (undir) { // when directed results, these layer configurations are not equivalent
    for (int i = 0; i < nrlayers_; i++){
      for (int j = i + 1; j < nrlayers_; j++){
        for (int k = 0; k < nrlayers_; k++){
          // Timing 112, i.e., for two layers 100 == 010 AND 101 == 011
          int lc_12_switch_a = i + j * nrlayers_ + k * (nrlayers_ * nrlayers_);
          int lc_12_switch_b = j + i * nrlayers_ + k * (nrlayers_ * nrlayers_);
          for (int m = 0; m < 5; m++) {
            trianglecounts(1, m, lc_12_switch_a) += trianglecounts(1, m, lc_12_switch_b);
            trianglecounts(1, m, lc_12_switch_b)  = trianglecounts(1, m, lc_12_switch_a);
          }
          // Timing 122, i.e., for two layers 010 == 001 AND 110 == 101
          int lc_23_switch_a = k + i * nrlayers_ + j * (nrlayers_ * nrlayers_);
          int lc_23_switch_b = k + j * nrlayers_ + i * (nrlayers_ * nrlayers_);
          for (int m = 0; m < 4; m++) {
            trianglecounts(2, m, lc_23_switch_a) += trianglecounts(2, m, lc_23_switch_b);
            trianglecounts(2, m, lc_23_switch_b)  = trianglecounts(2, m, lc_23_switch_a);
          }
        }
      }
    }
  }
}

//==================================================================================
// Two-node, i.e., edge motif, counting methods
void MultTempMotifCounter::AddMultEdgeData(TVec<TIntTriple>& ts_indices, TVec<MultEdgeData>& events,
                                           int& index, int u, int v, int dir) {
  if (HasEdges(u, v)) {
    const TIntV& ts_vec = temporal_data_[u].GetDat(v);
    const TIntV& layers = layer_data_[u].GetDat(v);
    const TIntV& papers = edge_attribute_data_[u].GetDat(v);
    for (int i = 0; i < ts_vec.Len(); ++i) {
      ts_indices.Add(TIntTriple(ts_vec[i], TIntPair(papers[i], index)));
      events.Add(MultEdgeData(dir, layers[i]));
      index++;
    }
  }
}

void MultTempMotifCounter::Count3MTEdge2Node(double delta, Counter7D& counts) {
  // Get a vector of undirected edges (so we can use openmp parallel for over it)
  TVec<TIntPair> undir_edges;
  for (TNGraph::TEdgeI it = static_graph_->BegEI(); it < static_graph_->EndEI(); it++) {
    int src = it.GetSrcNId();
    int dst = it.GetDstNId();
    // Only consider undirected edges
    if (src < dst || (dst < src && !static_graph_->IsEdge(dst, src))) {
      undir_edges.Add(TIntPair(src, dst));
    }
  }
  counts = Counter7D(4, 2, nrlayers_, 2, nrlayers_, 2, nrlayers_);

  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < undir_edges.Len(); i++) {
    TIntPair edge = undir_edges[i];
    Counter7D local;
    Count3MTEdge2Node(edge.Key, edge.Dat, delta, local, false);

    #pragma omp critical
    {
      for(int i = 0; i < 4; ++i){
        for (int d1 = 0; d1 < 2; ++d1) {
          for (int l1 = 0; l1 < nrlayers_; ++l1) {
            for (int d2 = 0; d2 < 2; ++d2) {
              for (int l2 = 0; l2 < nrlayers_; ++l2) {
                for (int d3 = 0; d3 < 2; ++d3) {
                  for (int l3 = 0; l3 < nrlayers_; ++l3) {
                    counts(i, d1, l1, d2, l2, d3, l3) += local(i, d1, l1, d2, l2, d3, l3);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

void MultTempMotifCounter::Count3MTEdge2Node(int u, int v, double delta, Counter7D& counts, bool deduct) {
  // Sort event list by time and paper id
  TVec<TIntTriple> ts_indices;
  TVec<MultEdgeData> events;
  int index = 0;
  AddMultEdgeData(ts_indices, events, index, u, v, 0);
  AddMultEdgeData(ts_indices, events, index, v, u, 1);
  ts_indices.Sort();

  TIntV timestamps;
  TVec<TVec<TVec<MultEdgeData> > > ordered_events;
  int i = 0, j = 0, m;
  TInt n;
  while ( i < ts_indices.Len() ){
    // Create a set for each timestamp
    timestamps.Add(ts_indices[i].Key);
    TVec<TIntPair> ps_indices;
    TVec<MultEdgeData> ts_events;
    index = 0;
    while ( i < ts_indices.Len() && timestamps[j] == ts_indices[i].Key ){
      ps_indices.Add(TIntPair(ts_indices[i].Dat.Key, index));
      ts_events.Add(events[ts_indices[i].Dat.Dat]);
      index++;
      i++;
    }
    j++;
    // Transform the timestamp collection into a set of equal edge attribute value sets
    ps_indices.Sort();
    TVec<TVec<MultEdgeData> > ts_ordered_events;
    m = 0;
    while ( m < ps_indices.Len() ){
      TVec<MultEdgeData> ps_events;
      n = ps_indices[m].Key;
      while ( m < ps_indices.Len() && ps_indices[m].Key == n ){
        ps_events.Add(ts_events[ps_indices[m].Dat]);
        m++;
      }
      // Add the set of edges per edge attribute value to the equal timestamp set of sets
      ts_ordered_events.Add(ps_events);
    }
    // Add the set of sets of edges per timestamp to the ordered events
    ordered_events.Add(ts_ordered_events);
  }

  if ( deduct) { // If counts are used to deduct from star counts
    ThreeMTEdgeMotifCounterToDeduct counter(nrlayers_);
    counter.Count(ordered_events, timestamps, delta, counts);
  } else { // If we are counting actual edge motifs
    ThreeMTEdgeMotifCounter counter(nrlayers_);
    counter.Count(ordered_events, timestamps, delta, counts);
  }
}

//==================================================================================
// Star counting methods
void MultTempMotifCounter::AddMultStarEdgeData(TVec<TIntTriple>& ts_indices,
                                       TVec<MultStarEdgeData>& events,
                                       int& index, int u, int v, int nbr, int dir) {
  if (HasEdges(u, v)) {
    const TIntV& ts_vec = temporal_data_[u].GetDat(v);
    const TIntV& layers = layer_data_[u].GetDat(v);
    const TIntV& papers = edge_attribute_data_[u].GetDat(v);
    for (int i = 0; i < ts_vec.Len(); ++i) {
      ts_indices.Add(TIntTriple(ts_vec[i], TIntPair(papers[i],index)));
      events.Add(MultStarEdgeData(nbr, dir, layers[i]));
      index++;
    }
  }
}

void MultTempMotifCounter::Count3MTEdge3NodeStars(double delta, Counter7D& starcounts) {
  TIntV centers;
  GetAllNodes(centers);
  starcounts = Counter7D(8, 2, nrlayers_, 2, nrlayers_, 2, nrlayers_);

  // Get counts for each node as the center
  #pragma omp parallel for schedule(dynamic)
  for (int c = 0; c < centers.Len(); c++) {
    // Gather all adjacent events
    int center = centers[c];
    TVec<TIntTriple> ts_indices;
    TVec<MultStarEdgeData> events;
    int index = 0;
    TIntV nbrs;
    GetAllNeighbors(center, nbrs);
    int nbr_index = 0;
    for (; nbr_index < nbrs.Len(); nbr_index++) {
      int nbr = nbrs[nbr_index];
      AddMultStarEdgeData(ts_indices, events, index, center, nbr, nbr_index, 0);
      AddMultStarEdgeData(ts_indices, events, index, nbr, center, nbr_index, 1);
    }
    ts_indices.Sort();

    TIntV timestamps;
    TVec<TVec<TVec<MultStarEdgeData> > > ordered_events;
    int j = 0, k = 0, m;
    TInt n;
    while ( k < ts_indices.Len() ){
      // Create a set for each timestamp
      timestamps.Add(ts_indices[k].Key);
      TVec<TIntPair> ps_indices;
      TVec<MultStarEdgeData> ts_events;
      index = 0;
      while ( k < ts_indices.Len() && timestamps[j] == ts_indices[k].Key ){
        ps_indices.Add(TIntPair(ts_indices[k].Dat.Key, index));
        ts_events.Add(events[ts_indices[k].Dat.Dat]);
        index++;
        k++;
      }
      j++;
      // Transform the timestamp set into a set of of equal edge attribute value sets
      ps_indices.Sort();
      TVec<TVec<MultStarEdgeData> > ts_ordered_events;
      m = 0;
      while ( m < ps_indices.Len() ){
        TVec<MultStarEdgeData> ps_events;
        n = ps_indices[m].Key;
        while ( m < ps_indices.Len() && ps_indices[m].Key == n ){
          ps_events.Add(ts_events[ps_indices[m].Dat]);
          m++;
        }
        // Add the set of edges per edge attribute value to the equal timestamp set of sets
        ts_ordered_events.Add(ps_events);
      }
      // Add the set of sets of edges per timestamp to the ordered events
      ordered_events.Add(ts_ordered_events);
    }

    ThreeMTEdgeStarCounter tesc(nbr_index+1, nrlayers_);
    tesc.Count(ordered_events, timestamps, delta);
    #pragma omp critical
    { // Update counts
      for (int d1 = 0; d1 < 2; ++d1) {
        for (int l1 = 0; l1 < nrlayers_; ++l1) {
          for (int d2 = 0; d2 < 2; ++d2) {
            for (int l2 = 0; l2 < nrlayers_; ++l2) {
              for (int d3 = 0; d3 < 2; ++d3) {
                for (int l3 = 0; l3 < nrlayers_; ++l3) {
                  starcounts(0, d1, l1, d2, l2, d3, l3) += tesc.ConcCount       (d1, l1, d2, l2, d3, l3);
                  starcounts(1, d1, l1, d2, l2, d3, l3) += tesc.PostPartialCount(d1, l1, d2, l2, d3, l3);
                  starcounts(2, d1, l1, d2, l2, d3, l3) += tesc.PostConcCount   (d1, l1, d2, l2, d3, l3);
                  starcounts(3, d1, l1, d2, l2, d3, l3) += tesc.PostCount       (d1, l1, d2, l2, d3, l3);
                  starcounts(4, d1, l1, d2, l2, d3, l3) += tesc.PreConcCount    (d1, l1, d2, l2, d3, l3);
                  starcounts(5, d1, l1, d2, l2, d3, l3) += tesc.PrePartialCount (d1, l1, d2, l2, d3, l3);
                  starcounts(6, d1, l1, d2, l2, d3, l3) += tesc.MidCount        (d1, l1, d2, l2, d3, l3);
                  starcounts(7, d1, l1, d2, l2, d3, l3) += tesc.PreCount        (d1, l1, d2, l2, d3, l3);
                }
              }
            }
          }
        }
      }
    }
    // Subtract edge-wise counts
    for (int nbr_id = 0; nbr_id < nbrs.Len(); nbr_id++) {
      int nbr = nbrs[nbr_id];
      Counter7D edge_counts;
      Count3MTEdge2Node(center, nbr, delta, edge_counts, true);
      #pragma omp critical
      {
        for (int d1 = 0; d1 < 2; ++d1) {
          for (int l1 = 0; l1 < nrlayers_; ++l1) {
            for (int d2 = 0; d2 < 2; ++d2) {
              for (int l2 = 0; l2 < nrlayers_; ++l2) {
                for (int d3 = 0; d3 < 2; ++d3) {
                  for (int l3 = 0; l3 < nrlayers_; ++l3) {
                    starcounts(0, d1, l1, d2, l2, d3, l3) -= edge_counts(0, d1, l1, d2, l2, d3, l3);
                    starcounts(1, d1, l1, d2, l2, d3, l3) -= edge_counts(4, d1, l1, d2, l2, d3, l3);
                    starcounts(2, d1, l1, d2, l2, d3, l3) -= edge_counts(2, d1, l1, d2, l2, d3, l3);
                    starcounts(3, d1, l1, d2, l2, d3, l3) -= edge_counts(3, d1, l1, d2, l2, d3, l3);
                    starcounts(4, d1, l1, d2, l2, d3, l3) -= edge_counts(1, d1, l1, d2, l2, d3, l3);
                    starcounts(5, d1, l1, d2, l2, d3, l3) -= edge_counts(5, d1, l1, d2, l2, d3, l3);
                    starcounts(6, d1, l1, d2, l2, d3, l3) -= edge_counts(3, d1, l1, d2, l2, d3, l3);
                    starcounts(7, d1, l1, d2, l2, d3, l3) -= edge_counts(3, d1, l1, d2, l2, d3, l3);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

//==================================================================================
// Triad counting methods
void MultTempMotifCounter::AddMultTriadEdgeData(TVec<TIntTriple>& ts_indices,
                                        TVec<MultTriadEdgeData>& events,
                                        int& index, int u, int v, int nbr,
                                        int dir, int u_or_v) {
  if (HasEdges(u, v)) {
    const TIntV& ts_vec = temporal_data_[u].GetDat(v);
    const TIntV& layers = layer_data_[u].GetDat(v);
    const TIntV& papers = edge_attribute_data_[u].GetDat(v);
    for (int i = 0; i < ts_vec.Len(); i++) {
      ts_indices.Add(TIntTriple(ts_vec[i], TIntPair(papers[i],index)));
      events.Add(MultTriadEdgeData(nbr, dir, u_or_v, layers[i]));
      ++index;
    }
  }
}

void MultTempMotifCounter::Count3MTEdgeTriads(double delta, Counter5D& counts) {
  counts = Counter5D(4, 8, nrlayers_, nrlayers_, nrlayers_);

  // Get the counts on each undirected, unlabelled edge
  TVec< THash<TInt, TInt> > edge_counts(static_graph_->GetMxNId());
  TVec< THash<TInt, TIntV> > assignments(static_graph_->GetMxNId());
  for (TNGraph::TEdgeI it = static_graph_->BegEI();
       it < static_graph_->EndEI(); it++) {
    int src = it.GetSrcNId();
    int dst = it.GetDstNId();
    int min_node = MIN(src, dst);
    int max_node = MAX(src, dst);
    edge_counts[min_node](max_node) += temporal_data_[src](dst).Len();
    assignments[min_node](max_node) = TIntV();
  }

  // Assign triangles to the edge with the most events
  TIntV Us, Vs, Ws;
  GetAllStaticTriangles(Us, Vs, Ws);
  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < Us.Len(); i++) {
    int u = Us[i];
    int v = Vs[i];
    int w = Ws[i];
    int counts_uv = edge_counts[MIN(u, v)].GetDat(MAX(u, v));
    int counts_uw = edge_counts[MIN(u, w)].GetDat(MAX(u, w));
    int counts_vw = edge_counts[MIN(v, w)].GetDat(MAX(v, w));
    if        (counts_uv >= MAX(counts_uw, counts_vw)) {
      #pragma omp critical
      {
        TIntV& assignment = assignments[MIN(u, v)].GetDat(MAX(u, v));
        assignment.Add(w);
      }
    } else if (counts_uw >= MAX(counts_uv, counts_vw)) {
      #pragma omp critical
      {
        TIntV& assignment = assignments[MIN(u, w)].GetDat(MAX(u, w));
        assignment.Add(v);
      }
    } else if (counts_vw >= MAX(counts_uv, counts_uw)) {
      #pragma omp critical
      {
        TIntV& assignment = assignments[MIN(v, w)].GetDat(MAX(v, w));
        assignment.Add(u);
      }
    }
  }

  TVec<TIntPair> all_edges;
  TIntV all_nodes;
  GetAllNodes(all_nodes);
  for (int node_id = 0; node_id < all_nodes.Len(); node_id++) {
    int u = all_nodes[node_id];
    TIntV nbrs;
    GetAllNeighbors(u, nbrs);
    for (int nbr_id = 0; nbr_id < nbrs.Len(); nbr_id++) {
      int v = nbrs[nbr_id];
      if (assignments[u].IsKey(v) && assignments[u].GetDat(v).Len() > 0) {
        all_edges.Add(TIntPair(u, v));
      }
    }
  }

  // Count triangles on edges with the assigned neighbours
  #pragma omp parallel for schedule(dynamic)
  for (int edge_id = 0; edge_id < all_edges.Len(); edge_id++) {
    TIntPair edge = all_edges[edge_id];
    int u = edge.Key;
    int v = edge.Dat;
    // Continue if no assignment
    if (!assignments[u].IsKey(v)) { continue; }
    TIntV& uv_assignment = assignments[u].GetDat(v);
    // Continue if no data
    if (uv_assignment.Len() == 0) { continue; }
    // Get all events on (u, v)
    TVec<TIntTriple> ts_indices;
    TVec<MultTriadEdgeData> events;
    int index = 0;
    int nbr_index = 0;
    // Assign indices from 0, 1, ..., num_nbrs + 2
    AddMultTriadEdgeData(ts_indices, events, index, u, v, nbr_index, 0, 1);
    nbr_index++;
    AddMultTriadEdgeData(ts_indices, events, index, v, u, nbr_index, 0, 0);
    nbr_index++;
    // Get all events on triangles assigned to (u, v)
    for (int w_id = 0; w_id < uv_assignment.Len(); w_id++) {
      int w = uv_assignment[w_id];
      AddMultTriadEdgeData(ts_indices, events, index, w, u, nbr_index, 0, 0);
      AddMultTriadEdgeData(ts_indices, events, index, w, v, nbr_index, 0, 1);
      AddMultTriadEdgeData(ts_indices, events, index, u, w, nbr_index, 1, 0);
      AddMultTriadEdgeData(ts_indices, events, index, v, w, nbr_index, 1, 1);
      nbr_index++;
    }
    // Put events in sorted order
    ts_indices.Sort();

    TIntV timestamps;
    TVec<TVec<TVec<MultTriadEdgeData> > > ordered_events;
    int j = 0, k = 0, m;
    TInt n;
    while ( k < ts_indices.Len() ){
      // Create a set for each timestamp
      timestamps.Add(ts_indices[k].Key);
      TVec<TIntPair> ps_indices;
      TVec<MultTriadEdgeData> ts_events;
      index = 0;
      while ( k < ts_indices.Len() && timestamps[j] == ts_indices[k].Key ){
        ps_indices.Add(TIntPair(ts_indices[k].Dat.Key, index));
        ts_events.Add(events[ts_indices[k].Dat.Dat]);
        index++;
        k++;
      }
      j++;
      // Transform the timestamp set into a set of of equal edge attribute value sets
      ps_indices.Sort();
      TVec<TVec<MultTriadEdgeData> > ts_ordered_events;
      m = 0;
      while ( m < ps_indices.Len() ){
        TVec<MultTriadEdgeData> ps_events;
        n = ps_indices[m].Key;
        while ( m < ps_indices.Len() && ps_indices[m].Key == n ){
          ps_events.Add(ts_events[ps_indices[m].Dat]);
          m++;
        }
        // Add the set of edges per edge attribute value to the equal timestamp set of sets
        ts_ordered_events.Add(ps_events);
      }
      // Add the set of sets of edges per timestamp to the ordered events
      ordered_events.Add(ts_ordered_events);
    }

    // Get the counts and update the counter
    ThreeMTEdgeTriadCounter tetc(nbr_index, 0, 1, nrlayers_);
    tetc.Count(ordered_events, timestamps, delta);
    #pragma omp critical
    {
      for (int lay1 = 0; lay1 < nrlayers_; ++lay1) {
        for (int lay2 = 0; lay2 < nrlayers_; ++lay2) {
          for (int lay3 = 0; lay3 < nrlayers_; ++lay3) {
            // Timing 111
            counts(0, 0, lay1, lay2, lay3) += tetc.ConcCount(0, 0, lay1, lay3, lay2)
                                            + tetc.ConcCount(1, 0, lay1, lay2, lay3)
                                            + tetc.ConcCount(1, 1, lay3, lay2, lay1);       // M_{t,1,1}
            counts(0, 1, lay1, lay2, lay3) += tetc.ConcCount(0, 1, lay1, lay2, lay3);       // M_{t,1,2}
            // Timing 112
            counts(1, 0, lay1, lay2, lay3) += tetc.PrePartialCount(1, 0, lay1, lay2, lay3); // M_{t,2,1}
            counts(1, 1, lay1, lay2, lay3) += tetc.PrePartialCount(0, 1, lay1, lay2, lay3); // M_{t,2,2}
            counts(1, 2, lay1, lay2, lay3) += tetc.PrePartialCount(1, 1, lay1, lay2, lay3); // M_{t,2,3}
            counts(1, 4, lay1, lay2, lay3) += tetc.PrePartialCount(0, 0, lay1, lay2, lay3); // M_{t,2,5}
            // Timing 122
            counts(2, 0, lay1, lay2, lay3) += tetc.PostPartialCount(1, 1, lay1, lay2, lay3);// M_{t,3,1}
            counts(2, 1, lay1, lay2, lay3) += tetc.PostPartialCount(0, 1, lay1, lay2, lay3);// M_{t,3,2}
            counts(2, 2, lay1, lay2, lay3) += tetc.PostPartialCount(1, 0, lay1, lay2, lay3);// M_{t,3,3}
            counts(2, 3, lay1, lay2, lay3) += tetc.PostPartialCount(0, 0, lay1, lay2, lay3);// M_{t,3,4}
            // Timing 123
            counts(3, 0, lay1, lay2, lay3) += tetc.SerialCount(0, lay1, 1, lay2, 0, lay3);  // M_{t,4,1}
            counts(3, 1, lay1, lay2, lay3) += tetc.SerialCount(0, lay1, 1, lay2, 1, lay3);  // M_{t,4,2}
            counts(3, 2, lay1, lay2, lay3) += tetc.SerialCount(0, lay1, 0, lay2, 0, lay3);  // M_{t,4,3}
            counts(3, 3, lay1, lay2, lay3) += tetc.SerialCount(0, lay1, 0, lay2, 1, lay3);  // M_{t,4,4}
            counts(3, 4, lay1, lay2, lay3) += tetc.SerialCount(1, lay1, 1, lay2, 0, lay3);  // M_{t,4,5}
            counts(3, 5, lay1, lay2, lay3) += tetc.SerialCount(1, lay1, 1, lay2, 1, lay3);  // M_{t,4,6}
            counts(3, 6, lay1, lay2, lay3) += tetc.SerialCount(1, lay1, 0, lay2, 0, lay3);  // M_{t,4,7}
            counts(3, 7, lay1, lay2, lay3) += tetc.SerialCount(1, lay1, 0, lay2, 1, lay3);  // M_{t,4,8}
          }
        }
      }
    }
  }
}

//==================================================================================
// Three-edge multilayer temporal edge motif counter
void ThreeMTEdgeMotifCounter::Count(const TVec<TVec<TVec<MultEdgeData> > >& event_string,
                                    const TIntV& timestamps, double delta, Counter7D& counts) {
  // Initialize everything to empty
  // Single edge counters
  conc_nodes_ = Counter2D(2, nrlayers_);
  tmp_nodes_  = Counter2D(2, nrlayers_);
  nodes_      = Counter2D(2, nrlayers_);
  // Two edge counters
  conc_sum_        = Counter4D(2, nrlayers_, 2, nrlayers_);
  tmp_sum_         = Counter4D(2, nrlayers_, 2, nrlayers_);
  pre_conc_sum_    = Counter4D(2, nrlayers_, 2, nrlayers_);
  pre_partial_sum_ = Counter4D(2, nrlayers_, 2, nrlayers_);
  tmp_pp_sum_      = Counter4D(2, nrlayers_, 2, nrlayers_);
  sum_             = Counter4D(2, nrlayers_, 2, nrlayers_);
  // 3-edge edge motif counters
  concurrent_   = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  pre_partial_  = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  post_partial_ = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  serial_       = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);

  if (event_string.Len() != timestamps.Len()) {
    TExcept::Throw("Number of events must equal number of timestamps");
  }
  int start = 0;
  for (int end = 0; end < event_string.Len(); end++) {
    while (double(timestamps[start]) + delta < double(timestamps[end])) {
      DecrementCounts(event_string[start]);
      start++;
    }
    IncrementCounts(event_string[end]);
  }

  counts = Counter7D(4, 2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  for (int d1 = 0; d1 < 2; ++d1) {
    for (int l1 = 0; l1 < nrlayers_; ++l1) {
      for (int d2 = 0; d2 < 2; ++d2) {
        for (int l2 = 0; l2 < nrlayers_; ++l2) {
          for (int d3 = 0; d3 < 2; ++d3) {
            for (int l3 = 0; l3 < nrlayers_; ++l3) {
              counts(0, d1, l1, d2, l2, d3, l3) = concurrent_(d1, l1, d2, l2, d3, l3);
              counts(1, d1, l1, d2, l2, d3, l3) = pre_partial_(d1, l1, d2, l2, d3, l3);
              counts(2, d1, l1, d2, l2, d3, l3) = post_partial_(d1, l1, d2, l2, d3, l3);
              counts(3, d1, l1, d2, l2, d3, l3) = serial_(d1, l1, d2, l2, d3, l3);
            }
          }
        }
      }
    }
  }
}

void ThreeMTEdgeMotifCounter::IncrementCounts(const TVec<TVec<MultEdgeData> >& events) {
  int dir, lay;
  for (int a = 0; a < events.Len(); a++){
    // Process the next batch of pid equal edges
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++) {
          for (int k = 0; k < 2; k++) {
            for (int l = 0; l < nrlayers_; l++) {
              concurrent_(i, j, k, l, dir, lay) += conc_sum_(i, j, k, l);
              pre_partial_(i, j, k, l, dir, lay) += pre_conc_sum_(i, j, k, l);
              post_partial_(i, j, k, l, dir, lay) += pre_partial_sum_(i, j, k, l);
              serial_(i, j, k, l, dir, lay) += sum_(i, j, k, l);
            }
          }
          tmp_sum_(i, j, dir, lay) += conc_nodes_(i, j);
          tmp_pp_sum_(i, j, dir, lay) += nodes_(i, j);
        }
      }
      tmp_nodes_(dir, lay) += 1;
    }
    // Update (and reset) the 'now' counters based on the most recent pid edges
    conc_sum_ = tmp_sum_;
    pre_partial_sum_ = tmp_pp_sum_;
    conc_nodes_ = tmp_nodes_;
  }
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < nrlayers_; j++) {
      for (int k = 0; k < 2; k++) {
        for (int l = 0; l < nrlayers_; l++) {
          pre_conc_sum_(i, j, k, l) += conc_sum_(i, j, k, l);
        }
      }
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++)
          sum_(i, j, dir, lay) += nodes_(i, j);
      }
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nodes_(events[a][b].dir, events[a][b].l)++;
    }
  }
  conc_nodes_.reset();
  tmp_nodes_.reset();
  conc_sum_.reset();
  tmp_sum_.reset();
  pre_partial_sum_.reset();
  tmp_pp_sum_.reset();
}

void ThreeMTEdgeMotifCounter::DecrementCounts(const TVec<TVec<MultEdgeData> >& events) {
  int dir, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nodes_(events[a][b].dir, events[a][b].l)--;
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++ ){
          sum_(dir, lay, i, j) -= nodes_(i, j);
          pre_conc_sum_(i, j, dir, lay) -= conc_nodes_(i, j);
        }
      }
      tmp_nodes_(dir, lay)++;
    }
    // Update (and reset) the 'now' counters based on the most recent pid edges
    conc_nodes_ = tmp_nodes_;
  }
  conc_nodes_.reset();
  tmp_nodes_.reset();
}

//==================================================================================
// Generic three temporal edge motif counter
void ThreeMTEdgeMotifCounterToDeduct::Count(const TVec<TVec<TVec<MultEdgeData> > >& event_string,
                                    const TIntV& timestamps, double delta, Counter7D& counts) {
  // Initialize everything to empty
  // Single edge counters
  conc_nodes_     = Counter2D(2, nrlayers_);
  tmp_nodes_      = Counter2D(2, nrlayers_);
  conc_pre_nodes_ = Counter2D(2, nrlayers_);
  nodes_          = Counter2D(2, nrlayers_);
  // Two edge counters
  conc_sum_         = Counter4D(2, nrlayers_, 2, nrlayers_);
  conc_sum_rev_     = Counter4D(2, nrlayers_, 2, nrlayers_);
  conc_mid_sum_     = Counter4D(2, nrlayers_, 2, nrlayers_);
  tmp_sum_          = Counter4D(2, nrlayers_, 2, nrlayers_);
  pre_conc_sum_     = Counter4D(2, nrlayers_, 2, nrlayers_);
  pre_conc_sum_rev_ = Counter4D(2, nrlayers_, 2, nrlayers_);
  pre_partial_sum_  = Counter4D(2, nrlayers_, 2, nrlayers_);
  tmp_pp_sum_       = Counter4D(2, nrlayers_, 2, nrlayers_);
  sum_              = Counter4D(2, nrlayers_, 2, nrlayers_);
  // 3-edge edge motif counters
  concurrent_       = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  pre_partial_      = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  pre_partial_rev_  = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  post_partial_     = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  post_partial_rev_ = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  serial_           = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);

  if (event_string.Len() != timestamps.Len()) {
    TExcept::Throw("Number of events must equal number of timestamps");
  }
  int start = 0;
  for (int end = 0; end < event_string.Len(); end++) {
    while (double(timestamps[start]) + delta < double(timestamps[end])) {
      DecrementCounts(event_string[start]);
      start++;
    }
    IncrementCounts(event_string[end]);
  }

  counts = Counter7D(6, 2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  for (int d1 = 0; d1 < 2; ++d1) {
    for (int l1 = 0; l1 < nrlayers_; ++l1) {
      for (int d2 = 0; d2 < 2; ++d2) {
        for (int l2 = 0; l2 < nrlayers_; ++l2) {
          for (int d3 = 0; d3 < 2; ++d3) {
            for (int l3 = 0; l3 < nrlayers_; ++l3) {
              counts(0, d1, l1, d2, l2, d3, l3) = concurrent_(d1, l1, d2, l2, d3, l3);
              counts(1, d1, l1, d2, l2, d3, l3) = pre_partial_(d1, l1, d2, l2, d3, l3);
              counts(2, d1, l1, d2, l2, d3, l3) = post_partial_(d1, l1, d2, l2, d3, l3);
              counts(3, d1, l1, d2, l2, d3, l3) = serial_(d1, l1, d2, l2, d3, l3);
              counts(4, d1, l1, d2, l2, d3, l3) = pre_partial_(d1, l1, d2, l2, d3, l3)
                                                + pre_partial_rev_(d1, l1, d2, l2, d3, l3);
              counts(5, d1, l1, d2, l2, d3, l3) = post_partial_(d1, l1, d2, l2, d3, l3)
                                                + post_partial_rev_(d1, l1, d2, l2, d3, l3);
            }
          }
        }
      }
    }
  }
}

void ThreeMTEdgeMotifCounterToDeduct::IncrementCounts(const TVec<TVec<MultEdgeData> >& events) {
  int dir, lay;
  for (int a = 0; a < events.Len(); a++){
    // Process the next batch of pid equal edges
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++) {
          for (int k = 0; k < 2; k++) {
            for (int l = 0; l < nrlayers_; l++) {
              concurrent_(i, j, k, l, dir, lay) += conc_sum_(i, j, k, l);
              pre_partial_(i, j, k, l, dir, lay) += pre_conc_sum_(i, j, k, l);
              pre_partial_rev_(i, j, k, l, dir, lay) += pre_conc_sum_rev_(i, j, k, l);
              post_partial_(i, j, k, l, dir, lay) += pre_partial_sum_(i, j, k, l);
              serial_(i, j, k, l, dir, lay) += sum_(i, j, k, l);
            }
          }
          tmp_sum_(i, j, dir, lay) += conc_nodes_(i, j);
          tmp_pp_sum_(i, j, dir, lay) += nodes_(i, j);
        }
      }
      tmp_nodes_(dir, lay) += 1;
    }
    // Update (and reset) the 'now' counters based on the most recent pid edges
    conc_sum_ = tmp_sum_;
    pre_partial_sum_ = tmp_pp_sum_;
    conc_nodes_ = tmp_nodes_;
  }
  // Reset local counters that need to be used again
  conc_pre_nodes_ = conc_nodes_;
  conc_nodes_.reset();
  tmp_nodes_.reset();
  tmp_sum_.reset();
  pre_partial_sum_.reset();
  tmp_pp_sum_.reset();
  // Reverse loop
  for (int a = events.Len() - 1; a >= 0; a--){
    // Process the next batch of pid equal edges
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++) {
          conc_mid_sum_(dir, lay, i, j) -= conc_nodes_(i, j);
        }
      }
      conc_pre_nodes_(dir, lay) -= 1;
    }
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++) {
          for (int k = 0; k < 2; k++) {
            for (int l = 0; l < nrlayers_; l++) {
              concurrent_(i, j, k, l, dir, lay) += conc_sum_rev_(i, j, k, l);
              concurrent_(i, j, k, l, dir, lay) += conc_mid_sum_(i, j, k, l);
              post_partial_rev_(i, j, k, l, dir, lay) += pre_partial_sum_(i, j, k, l);
            }
          }
          tmp_sum_(i, j, dir, lay) += conc_nodes_(i, j);
          tmp_pp_sum_(i, j, dir, lay) += nodes_(i, j);
        }
      }
      tmp_nodes_(dir, lay) += 1;
    }
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++) {
          conc_mid_sum_(i, j, dir, lay) += conc_pre_nodes_(i, j);
        }
      }
    }
    // Update (and reset) the 'now' counters based on the most recent pid edges
    conc_sum_rev_ = tmp_sum_;
    pre_partial_sum_ = tmp_pp_sum_;
    conc_nodes_ = tmp_nodes_;
  }

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < nrlayers_; j++) {
      for (int k = 0; k < 2; k++) {
        for (int l = 0; l < nrlayers_; l++) {
          pre_conc_sum_(i, j, k, l) += conc_sum_(i, j, k, l);
          pre_conc_sum_rev_(i, j, k, l) += conc_sum_rev_(i, j, k, l);
        }
      }
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++)
          sum_(i, j, dir, lay) += nodes_(i, j);
      }
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nodes_(events[a][b].dir, events[a][b].l)++;
    }
  }
  conc_nodes_.reset();
  tmp_nodes_.reset();
  conc_pre_nodes_.reset();
  conc_sum_.reset();
  conc_sum_rev_.reset();
  conc_mid_sum_.reset();
  tmp_sum_.reset();
  pre_partial_sum_.reset();
  tmp_pp_sum_.reset();
}

void ThreeMTEdgeMotifCounterToDeduct::DecrementCounts(const TVec<TVec<MultEdgeData> >& events) {
  int dir, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nodes_(events[a][b].dir, events[a][b].l)--;
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++ ){
          sum_(dir, lay, i, j) -= nodes_(i, j);
          pre_conc_sum_(i, j, dir, lay) -= conc_nodes_(i, j);
        }
      }
      tmp_nodes_(dir, lay)++;
    }
    // Update (and reset) the 'now' counters based on the most recent pid edges
    conc_nodes_ = tmp_nodes_;
  }
  // Reset local counters to be reused
  conc_nodes_.reset();
  tmp_nodes_.reset();
  for (int a = events.Len() - 1; a >= 0; a--){ // reverse
    for (int b = 0; b < events[a].Len(); b++){
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++ ){
          pre_conc_sum_rev_(i, j, dir, lay) -= conc_nodes_(i, j);
        }
      }
      tmp_nodes_(dir, lay)++;
    }
    // Update (and reset) the 'now' counters based on the most recent pid edges
    conc_nodes_ = tmp_nodes_;
  }
  conc_nodes_.reset();
  tmp_nodes_.reset();
}

//==================================================================================
// Generic three temporal edge, three node star and triad counter.
template <typename EdgeData>
void StarTriad3MTEdgeCounter<EdgeData>::Count(const TVec<TVec<TVec<EdgeData> > >& events,
                                             const TIntV& timestamps, double delta) {
  InitializeCounters();
  if (events.Len() != timestamps.Len()) {
    TExcept::Throw("Number of events must match number of timestamps.");
  }
  int start = 0;
  int end = 0;
  int L = timestamps.Len();
  for (int j = 0; j < L; j++) {
    double tj = double(timestamps[j]);
    // Adjust counts in pre-window [tj - delta, tj)
    while (start < L && double(timestamps[start]) < tj - delta) {
      PopPre(events[start]);
      start++;
    }
    // Adjust counts in post-window (tj, tj + delta]
    while (end < L && double(timestamps[end]) <= tj + delta) {
      PushPos(events[end]);
      end++;
    }
    // Move current event off post-window
    PopPos(events[j]);
    ProcessCurrent(events[j]);
    PushPre(events[j]);
  }
}

//==================================================================================
// Methods for the main sub-routine in the fast star counting algorithm.
void ThreeMTEdgeStarCounter::InitializeCounters() {
  // Single edge counters
  pre_nodes_      = Counter3D(2, nrlayers_, max_nodes_);
  post_nodes_     = Counter3D(2, nrlayers_, max_nodes_);
  conc_nodes_     = Counter3D(2, nrlayers_, max_nodes_);
  tmp_nodes_      = Counter3D(2, nrlayers_, max_nodes_);
  conc_pre_nodes_ = Counter3D(2, nrlayers_, max_nodes_);
  // Two edge counters
  conc_sum_         = Counter4D(2, nrlayers_, 2, nrlayers_);
  conc_mid_sum_     = Counter4D(2, nrlayers_, 2, nrlayers_);
  post_partial_sum_ = Counter4D(2, nrlayers_, 2, nrlayers_);
  post_conc_sum_    = Counter4D(2, nrlayers_, 2, nrlayers_);
  post_sum_         = Counter4D(2, nrlayers_, 2, nrlayers_);
  pre_conc_sum_     = Counter4D(2, nrlayers_, 2, nrlayers_);
  pre_partial_sum_  = Counter4D(2, nrlayers_, 2, nrlayers_);
  mid_sum_          = Counter4D(2, nrlayers_, 2, nrlayers_);
  pre_sum_          = Counter4D(2, nrlayers_, 2, nrlayers_);
  tmp_sum_          = Counter4D(2, nrlayers_, 2, nrlayers_);
  tmp_post_sum_     = Counter4D(2, nrlayers_, 2, nrlayers_);
  tmp_pre_sum_      = Counter4D(2, nrlayers_, 2, nrlayers_);
  // Three edge motif counters
  conc_         = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  post_partial_ = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  post_conc_    = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  post_         = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  pre_conc_     = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  pre_partial_  = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  mid_          = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
  pre_          = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
}

void ThreeMTEdgeStarCounter::PopPre(const TVec<TVec<MultStarEdgeData> >& events) {
  int nbr, dir, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      pre_nodes_(events[a][b].dir, events[a][b].l, events[a][b].nbr) -= 1;
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++){
          pre_sum_(dir, lay, i, j) -= pre_nodes_(i, j, nbr);
          pre_conc_sum_(i, j, dir, lay) -= conc_nodes_(i, j, nbr);
        }
      }
      tmp_nodes_(dir, lay, nbr) += 1;
    }
    conc_nodes_ = tmp_nodes_;
  }
  // Reset all local counters
  tmp_nodes_.reset();
  conc_nodes_.reset();
}

void ThreeMTEdgeStarCounter::PopPos(const TVec<TVec<MultStarEdgeData> >& events) {
  int nbr, dir, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      post_nodes_(events[a][b].dir, events[a][b].l, events[a][b].nbr) -= 1;
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++){
          post_sum_(dir, lay, i, j) -= post_nodes_(i, j, nbr);
          post_conc_sum_(i, j, dir, lay) -= conc_nodes_(i, j, nbr);
        }
      }
      tmp_nodes_(dir, lay, nbr) += 1;
    }
    conc_nodes_ = tmp_nodes_;
  }
  // Reset all local counters
  tmp_nodes_.reset();
  conc_nodes_.reset();
}

void ThreeMTEdgeStarCounter::PushPre(const TVec<TVec<MultStarEdgeData> >& events) {
  int nbr, dir, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++){
          pre_sum_(i, j, dir, lay) += pre_nodes_(i, j, nbr);
          pre_conc_sum_(i, j, dir, lay) += conc_nodes_(i, j, nbr);
        }
      }
      tmp_nodes_(dir, lay, nbr) += 1;
    }
    conc_nodes_ = tmp_nodes_;
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      pre_nodes_(events[a][b].dir, events[a][b].l, events[a][b].nbr) += 1;
    }
  }
  // Reset all local counters
  tmp_nodes_.reset();
  conc_nodes_.reset();
}

void ThreeMTEdgeStarCounter::PushPos(const TVec<TVec<MultStarEdgeData> >& events) {
  int nbr, dir, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++){
          post_sum_(i, j, dir, lay) += post_nodes_(i, j, nbr);
          post_conc_sum_(i, j, dir, lay) += conc_nodes_(i, j, nbr);
        }
      }
      tmp_nodes_(dir, lay, nbr) += 1;
    }
    conc_nodes_ = tmp_nodes_;
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      post_nodes_(events[a][b].dir, events[a][b].l, events[a][b].nbr) += 1;
    }
  }
  // Reset all local counters
  tmp_nodes_.reset();
  conc_nodes_.reset();
}

void ThreeMTEdgeStarCounter::ProcessCurrent(const TVec<TVec<MultStarEdgeData> >& events) {
  int nbr, dir, lay;
  // Decrement middle sum and prepare some now/two sum counters
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++){
          mid_sum_(i, j, dir, lay) -= pre_nodes_(i, j, nbr);
        }
      }
    }
  }
  // Update counts
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++) {
          for (int k = 0; k < 2; k++) {
            for (int l = 0; l < nrlayers_; l++) {
              conc_(i, j, k, l, dir, lay) += conc_sum_(i, j, k, l);                 // M_{s,1,x}
              post_partial_(i, j, dir, lay, k, l) += post_partial_sum_(i, j, k, l); // M_{s,2,x}
              post_conc_(dir, lay, i, j, k, l) += post_conc_sum_(i, j, k, l);       // M_{s,3,x}
              post_(dir, lay, i, j, k, l) += post_sum_(i, j, k, l);                 // M_{s,4,x}
              pre_conc_(i, j, k, l, dir, lay) += pre_conc_sum_(i, j, k, l);         // M_{s,5,x}
              pre_partial_(i, j, k, l, dir, lay) += pre_partial_sum_(i, j, k, l);   // M_{s,6,x}
              mid_(i, j, dir, lay, k, l) += mid_sum_(i, j, k, l);                   // M_{s,7,x}
              pre_(i, j, k, l, dir, lay) += pre_sum_(i, j, k, l);                   // M_{s,8,x}
            }
          }
          tmp_sum_(i, j, dir, lay) += conc_nodes_(i, j, nbr);
          tmp_post_sum_(dir, lay, i, j) += post_nodes_(i, j, nbr);
          tmp_pre_sum_(i, j, dir, lay) += pre_nodes_(i, j, nbr);
        }
      }
      tmp_nodes_(dir, lay, nbr) += 1;
    }
    // Update the counters based on the most recent pid edges
    conc_nodes_ = tmp_nodes_;
    conc_sum_ = tmp_sum_;
    post_partial_sum_ = tmp_post_sum_;
    pre_partial_sum_ = tmp_pre_sum_;
  }
  // Reset all local counters
  conc_pre_nodes_ = conc_nodes_;
  conc_nodes_.reset();
  tmp_nodes_.reset();
  conc_sum_.reset();
  tmp_sum_.reset();
  pre_partial_sum_.reset();
  tmp_pre_sum_.reset();
  post_partial_sum_.reset();
  tmp_post_sum_.reset();
  // Reverse loop
  for (int a = events.Len() - 1; a >= 0; a--){
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++){
          conc_mid_sum_(dir, lay, i, j) -= conc_nodes_(i, j, nbr);
        }
      }
      conc_pre_nodes_(dir, lay, nbr) -= 1;
    }
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++) {
          for (int k = 0; k < 2; k++) {
            for (int l = 0; l < nrlayers_; l++) {
              conc_(i, j, k, l, dir, lay) += conc_sum_(i, j, k, l);             // M_{s,1,x}
              conc_(i, j, k, l, dir, lay) += conc_mid_sum_(i, j, k, l);         // M_{s,1,x}
              post_partial_(i, j, dir, lay, k, l) += post_partial_sum_(i, j, k, l);     // M_{s,2,x}
              pre_partial_(i, j, k, l, dir, lay) += pre_partial_sum_(i, j, k, l);     // M_{s,6,x}
            }
          }
          tmp_sum_(i, j, dir, lay) += conc_nodes_(i, j, nbr);
          tmp_post_sum_(dir, lay, i, j) += post_nodes_(i, j, nbr);
          tmp_pre_sum_(i, j, dir, lay) += pre_nodes_(i, j, nbr);
        }
      }
      tmp_nodes_(dir, lay, nbr) += 1;
    }
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++){
          conc_mid_sum_(i, j, dir, lay) += conc_pre_nodes_(i, j, nbr);
        }
      }
    }
    // Update the counters based on the most recent pid edges
    conc_nodes_ = tmp_nodes_;
    conc_sum_ = tmp_sum_;
    post_partial_sum_ = tmp_post_sum_;
    pre_partial_sum_ = tmp_pre_sum_;
  }
  // Increment middle sum
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr = events[a][b].nbr;
      dir = events[a][b].dir;
      lay = events[a][b].l;
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < nrlayers_; j++)
          mid_sum_(dir, lay, i, j) += post_nodes_(i, j, nbr);
      }
    }
  }
  // Reset all local counters
  conc_nodes_.reset();
  conc_sum_.reset();
  pre_partial_sum_.reset();
  post_partial_sum_.reset();
  tmp_nodes_.reset();
  tmp_sum_.reset();
  tmp_pre_sum_.reset();
  tmp_post_sum_.reset();
  conc_pre_nodes_.reset();
  conc_mid_sum_.reset();
}

//==================================================================================
// Methods for the main sub-routine in the fast triangle counting algorithm.
void ThreeMTEdgeTriadCounter::InitializeCounters() {
  // Single edge counters
  pre_nodes_      = Counter4D(2, nrlayers_, 2, max_nodes_);
  pos_nodes_      = Counter4D(2, nrlayers_, 2, max_nodes_);
  conc_nodes_     = Counter4D(2, nrlayers_, 2, max_nodes_);
  tmp_nodes_      = Counter4D(2, nrlayers_, 2, max_nodes_);
  conc_pre_nodes_ = Counter4D(2, nrlayers_, 2, max_nodes_);
  // Two edge counters
  pre_sum_          = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  post_sum_         = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  mid_sum_          = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  conc_sum_         = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  conc_mid_sum_     = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  pre_conc_sum_     = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  post_conc_sum_    = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  pre_partial_sum_  = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  post_partial_sum_ = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  tmp_sum_          = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  tmp_pre_sum_      = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  tmp_post_sum_     = Counter5D(2, 2, nrlayers_, 2, nrlayers_);
  // Three edge motif counters
  conc_         = Counter5D(2, 2, nrlayers_, nrlayers_, nrlayers_);
  pre_partial_  = Counter5D(2, 2, nrlayers_, nrlayers_, nrlayers_);
  post_partial_ = Counter5D(2, 2, nrlayers_, nrlayers_, nrlayers_);
  serial_       = Counter6D(2, nrlayers_, 2, nrlayers_, 2, nrlayers_);
}

void ThreeMTEdgeTriadCounter::PopPre(const TVec<TVec<MultTriadEdgeData> >& events) {
  int nbr, dir, u_or_v, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      if (!IsEdgeNode(events[a][b].nbr))
        pre_nodes_(events[a][b].dir, events[a][b].l, events[a][b].u_or_v, events[a][b].nbr) -= 1;
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (!IsEdgeNode(nbr)) {
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            pre_sum_(u_or_v, dir, lay, i, j) -= pre_nodes_(i, j, 1 - u_or_v, nbr);
            pre_conc_sum_(1 - u_or_v, i, j, dir, lay) -= conc_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
        tmp_nodes_(dir, lay, u_or_v, nbr) += 1;
      }
    }
    conc_nodes_ = tmp_nodes_;
  }
  // Reset all local counters
  tmp_nodes_.reset();
  conc_nodes_.reset();
}

void ThreeMTEdgeTriadCounter::PopPos(const TVec<TVec<MultTriadEdgeData> >& events) {
  int nbr, dir, u_or_v, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      if (!IsEdgeNode(events[a][b].nbr))
        pos_nodes_(events[a][b].dir, events[a][b].l, events[a][b].u_or_v, events[a][b].nbr) -= 1;
    }
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (!IsEdgeNode(nbr)) {
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            post_sum_(u_or_v, dir, lay, i, j) -= pos_nodes_(i, j, 1 - u_or_v, nbr);
            post_conc_sum_(1 - u_or_v, i, j, dir, lay) -= conc_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
        tmp_nodes_(dir, lay, u_or_v, nbr) += 1;
      }
    }
    conc_nodes_ = tmp_nodes_;
  }
  // Reset all local counters
  tmp_nodes_.reset();
  conc_nodes_.reset();
}

void ThreeMTEdgeTriadCounter::PushPre(const TVec<TVec<MultTriadEdgeData> >& events) {
  int nbr, dir, u_or_v, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (!IsEdgeNode(nbr)) {
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            pre_sum_(1 - u_or_v, i, j, dir, lay) += pre_nodes_(i, j, 1 - u_or_v, nbr);
            pre_conc_sum_(1 - u_or_v, i, j, dir, lay) += conc_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
        tmp_nodes_(dir, lay, u_or_v, nbr) += 1;
      }
    }
    conc_nodes_ = tmp_nodes_;
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      if (!IsEdgeNode(events[a][b].nbr))
        pre_nodes_(events[a][b].dir, events[a][b].l, events[a][b].u_or_v, events[a][b].nbr) += 1;
    }
  }
  // Reset all local counters
  tmp_nodes_.reset();
  conc_nodes_.reset();
}

void ThreeMTEdgeTriadCounter::PushPos(const TVec<TVec<MultTriadEdgeData> >& events) {
  int nbr, dir, u_or_v, lay;
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (!IsEdgeNode(nbr)) {
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            post_sum_(1 - u_or_v, i, j, dir, lay) += pos_nodes_(i, j, 1 - u_or_v, nbr);
            post_conc_sum_(1 - u_or_v, i, j, dir, lay) += conc_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
        tmp_nodes_(dir, lay, u_or_v, nbr) += 1;
      }
    }
    conc_nodes_ = tmp_nodes_;
  }
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      if (!IsEdgeNode(events[a][b].nbr))
        pos_nodes_(events[a][b].dir, events[a][b].l, events[a][b].u_or_v, events[a][b].nbr) += 1;
    }
  }
  // Reset all local counters
  tmp_nodes_.reset();
  conc_nodes_.reset();
}

void ThreeMTEdgeTriadCounter::ProcessCurrent(const TVec<TVec<MultTriadEdgeData> >& events) {
  int nbr, dir, u_or_v, lay;
  // Update mid_sum counter
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (!IsEdgeNode(nbr)) {
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            mid_sum_(1 - u_or_v, i, j, dir, lay) -= pre_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
      }
    }
  }
  // Update counts
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (IsEdgeNode(nbr)) {
        // Determine if the event edge is u --> v or v --> u
        int u_to_v = 0;
        if (((nbr == node_u_) && dir == 0) || ((nbr == node_v_) && dir == 1)) {
          u_to_v = 1;
        }
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < 2; j++) {
            for (int k = 0; k < nrlayers_; k++) {
              for (int l = 0; l < nrlayers_; l++) {
                conc_(i, j, k, l, lay) += conc_sum_(1 - u_to_v, i, k, j, l)
                                        + conc_sum_(    u_to_v, j, l, i, k);          // M_{t,1,x}
                pre_partial_(i, j, k, l, lay) += pre_conc_sum_(1 - u_to_v, i, k, j, l)
                                               + pre_conc_sum_(    u_to_v, j, l, i, k);  // M_{t,2,x}
                pre_partial_(i, j, k, lay, l) += post_partial_sum_((u_to_v == j), 1 - i, k, 0, l);
                pre_partial_(i, j, lay, k, l) += post_partial_sum_((u_to_v == i), 1 - j, k, 1, l);
                post_partial_(i, j, lay, k, l) += post_conc_sum_(1 - u_to_v, i, k, j, l)
                                                + post_conc_sum_(    u_to_v, j, l, i, k);  // M_{t,3,x}
                post_partial_(i, j, k, lay, l) += pre_partial_sum_((u_to_v != i), 1, k, 1 - j, l);
                post_partial_(i, j, k, l, lay) += pre_partial_sum_((u_to_v != j), 0, k, 1 - i, l);
              }
            }
          }
        }
        for (int i = 0; i < nrlayers_; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            // i --> j, k --> j, i --> k
            serial_(0,   i, 0, lay, 0,   j) += mid_sum_(    u_to_v, 0, i, 0, j); // M_{t,4,3}
            serial_(0, lay, 0,   i, 0,   j) += post_sum_(    u_to_v, 0, i, 1, j);
            serial_(0,   i, 0,   j, 0, lay) += pre_sum_(1 - u_to_v, 1, i, 1, j);
            // i --> j, k --> i, j --> k
            serial_(1,   i, 0, lay, 0,   j) += mid_sum_(    u_to_v, 1, i, 0, j); // M_{t,4,7}
            serial_(1, lay, 0,   i, 0,   j) += post_sum_(1 - u_to_v, 0, i, 1, j);
            serial_(1,   i, 0,   j, 0, lay) += pre_sum_(1 - u_to_v, 0, i, 1, j);
            // i --> j, j --> k, i --> k
            serial_(0,   i, 1, lay, 0,   j) += mid_sum_(1 - u_to_v, 0, i, 0, j); // M_{t,4,1}
            serial_(0, lay, 1,   i, 0,   j) += post_sum_(    u_to_v, 1, i, 1, j);
            serial_(0,   i, 1,   j, 0, lay) += pre_sum_(1 - u_to_v, 1, i, 0, j);
            // i --> j, i --> k, j --> k
            serial_(1,   i, 1, lay, 0,   j) += mid_sum_(1 - u_to_v, 1, i, 0, j); // M_{t,4,5}
            serial_(1, lay, 1,   i, 0,   j) += post_sum_(1 - u_to_v, 1, i, 1, j);
            serial_(1,   i, 1,   j, 0, lay) += pre_sum_(1 - u_to_v, 0, i, 0, j);
            // i --> j, k --> j, k --> i
            serial_(0,   i, 0, lay, 1,   j) += mid_sum_(    u_to_v, 0, i, 1, j); // M_{t,4,4}
            serial_(0, lay, 0,   i, 1,   j) += post_sum_(    u_to_v, 0, i, 0, j);
            serial_(0,   i, 0,   j, 1, lay) += pre_sum_(    u_to_v, 1, i, 1, j);
            // i --> j, k --> i, k --> j
            serial_(1,   i, 0, lay, 1,   j) += mid_sum_(    u_to_v, 1, i, 1, j); // M_{t,4,8}
            serial_(1, lay, 0,   i, 1,   j) += post_sum_(1 - u_to_v, 0, i, 0, j);
            serial_(1,   i, 0,   j, 1, lay) += pre_sum_(    u_to_v, 0, i, 1, j);
            // i --> j, j --> k, k --> i
            serial_(0,   i, 1, lay, 1,   j) += mid_sum_(1 - u_to_v, 0, i, 1, j); // M_{t,4,2}
            serial_(0, lay, 1,   i, 1,   j) += post_sum_(    u_to_v, 1, i, 0, j);
            serial_(0,   i, 1,   j, 1, lay) += pre_sum_(    u_to_v, 1, i, 0, j);
            // i --> j, i --> k, k --> j
            serial_(1,   i, 1, lay, 1,   j) += mid_sum_(1 - u_to_v, 1, i, 1, j); // M_{t,4,6}
            serial_(1, lay, 1,   i, 1,   j) += post_sum_(1 - u_to_v, 1, i, 0, j);
            serial_(1,   i, 1,   j, 1, lay) += pre_sum_(    u_to_v, 0, i, 0, j);
          }
        }
      } else { // nbr is neither u nor v
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            tmp_sum_(1 - u_or_v, i, j, dir, lay) += conc_nodes_(i, j, 1 - u_or_v, nbr);
            tmp_post_sum_(u_or_v, dir, lay, i, j) += pos_nodes_(i, j, 1 - u_or_v, nbr);
            tmp_pre_sum_(1 - u_or_v, i, j, dir, lay) += pre_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
        tmp_nodes_(dir, lay, u_or_v, nbr) += 1;
      }
    }
    conc_nodes_ = tmp_nodes_;
    conc_sum_ = tmp_sum_;
    post_partial_sum_ = tmp_post_sum_;
    pre_partial_sum_ = tmp_pre_sum_;
  }
  // Reset all local counters
  conc_pre_nodes_ = conc_nodes_;
  conc_nodes_.reset();
  tmp_nodes_.reset();
  conc_sum_.reset();
  tmp_sum_.reset();
  pre_partial_sum_.reset();
  tmp_pre_sum_.reset();
  post_partial_sum_.reset();
  tmp_post_sum_.reset();
  for (int a = events.Len() - 1; a >= 0; a--){ //Reverse loop
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (!IsEdgeNode(nbr)) {
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            conc_mid_sum_(u_or_v, dir, lay, i, j) -= conc_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
        conc_pre_nodes_(dir, lay, u_or_v, nbr) -= 1;
      }
    }
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (IsEdgeNode(nbr)) {
        // Determine if the event edge is u --> v or v --> u
        int u_to_v = 0;
        if (((nbr == node_u_) && dir == 0) || ((nbr == node_v_) && dir == 1)) {
          u_to_v = 1;
        }
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < 2; j++) {
            for (int k = 0; k < nrlayers_; k++) {
              for (int l = 0; l < nrlayers_; l++) {
                conc_(i, j, k, l, lay) += conc_sum_(1 - u_to_v, i, k, j, l)
                                        + conc_sum_(    u_to_v, j, l, i, k);          // M_{t,1,x}
                conc_(i, j, k, l, lay) += conc_mid_sum_(1 - u_to_v, i, k, j, l)
                                        + conc_mid_sum_(    u_to_v, j, l, i, k);      // M_{t,1,x}
                pre_partial_(i, j, k, lay, l) += post_partial_sum_((u_to_v == j), 1 - i, k, 0, l); // M_{t,2,x}
                pre_partial_(i, j, lay, k, l) += post_partial_sum_((u_to_v == i), 1 - j, k, 1, l);
                post_partial_(i, j, k, lay, l) += pre_partial_sum_((u_to_v != i), 1, k, 1 - j, l); // M_{t,3,x}
                post_partial_(i, j, k, l, lay) += pre_partial_sum_((u_to_v != j), 0, k, 1 - i, l);
              }
            }
          }
        }
      } else { // nbr is neither u nor v
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            tmp_sum_(1 - u_or_v, i, j, dir, lay) += conc_nodes_(i, j, 1 - u_or_v, nbr);
            tmp_post_sum_(u_or_v, dir, lay, i, j) += pos_nodes_(i, j, 1 - u_or_v, nbr);
            tmp_pre_sum_(1 - u_or_v, i, j, dir, lay) += pre_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
        tmp_nodes_(dir, lay, u_or_v, nbr) += 1;
      }
    }
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (!IsEdgeNode(nbr)) {
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            conc_mid_sum_(1 - u_or_v, i, j, dir, lay) += conc_pre_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
      }
    }
    conc_nodes_ = tmp_nodes_;
    conc_sum_ = tmp_sum_;
    post_partial_sum_ = tmp_post_sum_;
    pre_partial_sum_ = tmp_pre_sum_;
  }
  // Update mid_sum counter
  for (int a = 0; a < events.Len(); a++){
    for (int b = 0; b < events[a].Len(); b++){
      nbr    = events[a][b].nbr;
      dir    = events[a][b].dir;
      u_or_v = events[a][b].u_or_v;
      lay    = events[a][b].l;
      if (!IsEdgeNode(nbr)) {
        for (int i = 0; i < 2; i++) {
          for (int j = 0; j < nrlayers_; j++) {
            mid_sum_(u_or_v, dir, lay, i, j) += pos_nodes_(i, j, 1 - u_or_v, nbr);
          }
        }
      }
    }
  }
  // Reset all local counters
  conc_nodes_.reset();
  conc_sum_.reset();
  pre_partial_sum_.reset();
  post_partial_sum_.reset();
  tmp_nodes_.reset();
  tmp_sum_.reset();
  tmp_pre_sum_.reset();
  tmp_post_sum_.reset();
  conc_pre_nodes_.reset();
  conc_mid_sum_.reset();
}