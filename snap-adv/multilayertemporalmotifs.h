#ifndef snap_multilayertemporalmotifs_h
#define snap_multilayertemporalmotifs_h

#include "Snap.h"

typedef TKeyDat<TInt, TInt> TIntPair;
typedef TKeyDat<TInt, TIntPair> TIntTriple;

#ifndef snap_temporalmotifs_h

// Simple one-dimensional, two-dimensional, and up to seven-dimensional counter
// classes with default intialization.
class Counter1D {
 public:
  Counter1D(int m=0) : m_(m) {
    if (m > 0) {
      data_ = TVec<TInt64>(m);
      data_.PutAll(0);
    }
  }
  const TInt64& operator()(int i) const { return data_[i]; }
  TInt64& operator()(int i) { return data_[i]; }
  int m() { return m_; }
  void reset() { data_.PutAll(0); }

 private:
  int m_;
  TVec<TInt64> data_;
};

class Counter2D {
 public:
  Counter2D(int m=0, int n=0) : m_(m), n_(n) {
    if (m * n > 0) {
      data_ = TVec<TInt64>(m * n);
      data_.PutAll(0);
    }
  }
  const TInt64& operator()(int i, int j) const { return data_[i + j * m_]; }
  TInt64& operator()(int i, int j) { return data_[i + j * m_]; }
  int m() { return m_; }
  int n() { return n_; }
  void reset() { data_.PutAll(0); }

 private:
  int m_;
  int n_;
  TVec<TInt64> data_;
};

class Counter3D {
 public:
  Counter3D(int m=0, int n=0, int p=0) : m_(m), n_(n), p_(p) {
    if (m * n * p > 0) {
      data_ = TVec<TInt64>(m * n * p);
      data_.PutAll(0);
    }
  }
  const TInt64& operator()(int i, int j, int k) const {
    return data_[i + j * n_ + k * m_ * n_];
  }
  TInt64& operator()(int i, int j, int k) {
    return data_[i + j * m_ + k * m_ * n_];
  }
  int m() { return m_; }
  int n() { return n_; }
  int p() { return p_; }
  void reset() { data_.PutAll(0); }

 private:
  int m_;
  int n_;
  int p_;
  TVec<TInt64> data_;
};

#endif

class Counter4D {
 public:
  Counter4D(int m=0, int n=0, int p=0, int q=0) : m_(m), n_(n), p_(p), q_(q) {
    if (m * n * p * q > 0) {
      data_ = TVec<TInt64>(m * n * p * q);
      data_.PutAll(0);
    }
  }
  const TInt64& operator()(int i, int j, int k, int l) const {
    return data_[i + j * n_ + k * m_ * n_ + l * m_ * n_ * p_];
  }
  TInt64& operator()(int i, int j, int k, int l) {
    return data_[i + j * m_ + k * m_ * n_ + l * m_ * n_ * p_];
  }
  int m() { return m_; }
  int n() { return n_; }
  int p() { return p_; }
  int q() { return q_; }
  void reset() { data_.PutAll(0); }

 private:
  int m_;
  int n_;
  int p_;
  int q_;
  TVec<TInt64> data_;
};

class Counter5D {
 public:
  Counter5D(int m=0, int n=0, int p=0, int q=0, int r=0) : m_(m), n_(n), p_(p), q_(q), r_(r)
  {
    if (m * n * p * q * r > 0) {
      data_ = TVec<TInt64>(m * n * p * q * r);
      data_.PutAll(0);
    }
  }
  const TInt64& operator()(int i, int j, int k, int l, int m) const {
    return data_[i + j * n_ + k * m_ * n_ + l * m_ * n_ * p_ + m * m_ * n_ * p_ * q_];
  }
  TInt64& operator()(int i, int j, int k, int l, int m) {
    return data_[i + j * m_ + k * m_ * n_ + l * m_ * n_ * p_ + m * m_ * n_ * p_ * q_];
  }
  int m() { return m_; }
  int n() { return n_; }
  int p() { return p_; }
  int q() { return q_; }
  int r() { return r_; }
  void reset() { data_.PutAll(0); }

 private:
  int m_;
  int n_;
  int p_;
  int q_;
  int r_;
  TVec<TInt64> data_;
};

class Counter6D {
 public:
  Counter6D(int m=0, int n=0, int p=0, int q=0, int r=0, int s=0) : m_(m), n_(n), p_(p), q_(q), r_(r), s_(s)
  {
    if (m * n * p * q * r * s > 0) {
      data_ = TVec<TInt64>(m * n * p * q * r * s);
      data_.PutAll(0);
    }
  }
  const TInt64& operator()(int i, int j, int k, int l, int m, int n) const {
    return data_[i + j * n_ + k * m_ * n_ + l * m_ * n_ * p_ + m * m_ * n_ * p_ * q_ + n * m_ * n_ * p_ * q_ * r_];
  }
  TInt64& operator()(int i, int j, int k, int l, int m, int n) {
    return data_[i + j * m_ + k * m_ * n_ + l * m_ * n_ * p_ + m * m_ * n_ * p_ * q_ + n * m_ * n_ * p_ * q_ * r_];
  }
  int m() { return m_; }
  int n() { return n_; }
  int p() { return p_; }
  int q() { return q_; }
  int r() { return r_; }
  int s() { return s_; }
  void reset() { data_.PutAll(0); }

 private:
  int m_;
  int n_;
  int p_;
  int q_;
  int r_;
  int s_;
  TVec<TInt64> data_;
};

class Counter7D {
 public:
  Counter7D(int m=0, int n=0, int p=0, int q=0, int r=0, int s=0, int t=0) : m_(m), n_(n), p_(p), q_(q), r_(r), s_(s), t_(t)
  {
    if (m * n * p * q * r * s * t > 0) {
      data_ = TVec<TInt64>(m * n * p * q * r * s * t);
      data_.PutAll(0);
    }
  }
  const TInt64& operator()(int i, int j, int k, int l, int m, int n, int o) const {
    return data_[i + j * n_ + k * m_ * n_ + l * m_ * n_ * p_ + m * m_ * n_ * p_ * q_ + n * m_ * n_ * p_ * q_ * r_ + o * m_ * n_ * p_ * q_ * r_ * s_];
  }
  TInt64& operator()(int i, int j, int k, int l, int m, int n, int o) {
    return data_[i + j * m_ + k * m_ * n_ + l * m_ * n_ * p_ + m * m_ * n_ * p_ * q_ + n * m_ * n_ * p_ * q_ * r_ + o * m_ * n_ * p_ * q_ * r_ * s_];
  }
  int m() { return m_; }
  int n() { return n_; }
  int p() { return p_; }
  int q() { return q_; }
  int r() { return r_; }
  int s() { return s_; }
  int t() { return t_; }
  void reset() { data_.PutAll(0); }

 private:
  int m_;
  int n_;
  int p_;
  int q_;
  int r_;
  int s_;
  int t_;
  TVec<TInt64> data_;
};

//==================================================================================

// Edge data consists of a direction, and a layer.
class MultEdgeData {
 public:
  MultEdgeData() {}
  MultEdgeData(int _dir, int _l) : dir(_dir), l(_l) {}
  int dir;  // Outgoing (0) or incoming (1) direction
  int l;    // Layer
};

// Star edge data consists of a neighbour, a direction, and a layer.
class MultStarEdgeData {
 public:
  MultStarEdgeData() {}
  MultStarEdgeData(int _nbr, int _dir, int _l) : nbr(_nbr), dir(_dir), l(_l) {}
  int nbr;  // Which neighbor of the center node
  int dir;  // Outgoing (0) from or incoming (1) to the center node
  int l;    // Layer
};

// Triad edge data consists of a neighbour, a direction, an indicator of whether
// the edge connects with which endpoint (u or v), and a layer.
class MultTriadEdgeData {
 public:
  MultTriadEdgeData() {}
  MultTriadEdgeData(int _nbr, int _dir, int _u_or_v, int _l) :
      nbr(_nbr), dir(_dir), u_or_v(_u_or_v), l(_l) {}
  int nbr;     // Which neighbour of the center node
  int dir;     // Outgoing (0) from or incoming (1) to the neighbour node
  int u_or_v;  // Points to first end point u (0) or second end point v (1)
  int l;       // Layer
};

//==================================================================================

// Main multilayer temporal motif counting class. This implementation has support
// for counting motifs with three multilayer temporal edges on two or three nodes.
class MultTempMotifCounter {
 public:
  // Reads directed multilayer temporal graph data from the specified file,
  // which must have the following format:
  //    source_node destination_node unix_timestamp layer_id edge_attribute_value
  // Note that both timestamps and layer_id's must be positive integers
  // preferably the layer_id's should be 0,1,...,nrlayers-1 without gaps.
  // If ml is set to false, layer information is ignored,
  //   i.e., the execution assumes every edge belong to a single layer
  // If eae is set to true, edge attribute exclusivity will be enforced.
  // Note that we ALWAYS expect all columns even when unused based on flags set!
  MultTempMotifCounter(const TStr& filename, bool ml, bool eae);

  // Counts all 3-edge multilayer temporal edge motifs and stores results
  // in counter counts.  The format is:
  //   counts(0, ...): timing 111, all concurrent edges
  //                   (M_{e,1,1} and M_{e,1,2})
  //   counts(1, ...): timing 112, first two edge concurrent
  //                   (M_{e,2,1}, M_{e,2,2} and M_{e,5,2})
  //   counts(2, ...): timing 122, last two edge concurrent
  //                   (M_{e,3,1}, M_{e,3,2} and M_{e,6,2})
  //   counts(3, ...): timing 123,
  //                   (M_{e,4,1}, M_{e,4,2}, M_{e,7,2} and M_{e,8,2})
  // Given layers a,b,c we have direction format:
  //   counts(x, 0, a, 0, b, 0, c): u --> v, u --> v, u --> v
  //   counts(x, 1, a, 1, b, 1, c): v --> u, v --> u, v --> u
  //   counts(x, 0, a, 1, b, 1, c): u --> v, v --> u, v --> u
  //   counts(x, 1, a, 0, b, 0, c): v --> u, u --> v, u --> v
  //   counts(x, 0, a, 1, b, 0, c): u --> v, v --> u, u --> v
  //   counts(x, 1, a, 0, b, 1, c): v --> u, u --> v, v --> u
  //   counts(x, 0, a, 0, b, 1, c): u --> v, u --> v, v --> u
  //   counts(x, 1, a, 1, b, 0, c): v --> u, v --> u, u --> v
  void Count3MTEdge2Node(double delta, Counter7D& counts);

  // Similar to Count3MTEdge2Node() except only counts motif instances for a
  // given pair of nodes u and v and specifies the source and destination node.
  // If deduct is true, some counts are already resolved for layer equivalences
  //  such that they can be used to deduct from the star motif counts.
  void Count3MTEdge2Node(int u, int v, double delta, Counter7D& counts, bool deduct);

  // Counts 3-node, 3-edge star motifs and places the results in the
  // counter starcounts such that (with center node c):
  //
  //  starcount(0, ...): {c, u, 1}, {c, u, 1}, {c, v, 1}  // M_{s,1,x}
  //  starcount(1, ...): {c, u, 1}, {c, v, 1}, {c, u, 2}  // M_{s,2,x}
  //  starcount(2, ...): {c, v, 1}, {c, u, 2}, {c, u, 2}  // M_{s,3,x}
  //  starcount(3, ...): {c, v, 1}, {c, u, 2}, {c, u, 3}  // M_{s,4,x}
  //  starcount(4, ...): {c, u, 1}, {c, u, 1}, {c, v, 2}  // M_{s,5,x}
  //  starcount(5, ...): {c, u, 1}, {c, u, 2}, {c, v, 2}  // M_{s,6,x}
  //  starcount(6, ...): {c, u, 1}, {c, v, 2}, {c, u, 3}  // M_{s,7,x}
  //  starcount(7, ...): {c, u, 1}, {c, u, 2}, {c, v, 3}  // M_{s,8,x}
  //
  // The indices in the counter correspond to the direction with the center
  // node as the reference (0 indexes outgoing and 1 indexes incoming edges to
  // the center).  For example, given layers a,b,c:
  //  starcounts(3, 0, a, 1, b, 0, c): c --> v, u --> c, c --> u
  void Count3MTEdge3NodeStars(double delta, Counter7D& starcounts);

  // Counts 3-edge triad events and places the result in counts such that:
  //
  //  counts(0, 0, ...) -> M_{t,1,1}
  //  counts(0, 1, ...) -> M_{t,1,2}
  //
  //  counts(1, 0, ...) -> M_{t,2,1}
  //  counts(1, 1, ...) -> M_{t,2,2}
  //  counts(1, 2, ...) -> M_{t,2,3}
  //  counts(1, 4, ...) -> M_{t,2,5}
  //
  //  counts(2, 0, ...) -> M_{t,3,1}
  //  counts(2, 1, ...) -> M_{t,3,2}
  //  counts(2, 2, ...) -> M_{t,3,3}
  //  counts(2, 3, ...) -> M_{t,3,4}
  //
  //  counts(3, 0, ...) -> M_{t,4,1}
  //  counts(3, 1, ...) -> M_{t,4,2}
  //  counts(3, 2, ...) -> M_{t,4,3}
  //  counts(3, 3, ...) -> M_{t,4,4}
  //  counts(3, 4, ...) -> M_{t,4,5}
  //  counts(3, 5, ...) -> M_{t,4,6}
  //  counts(3, 6, ...) -> M_{t,4,7}
  //  counts(3, 7, ...) -> M_{t,4,8}
  //
  void Count3MTEdgeTriads(double delta, Counter5D& counts);

  // Counts all 2-node and 3-node, 3-edge multilayer temporal motifs and
  // places the result in counters counts, starcounts and trianglecounts
  // for respectively edge, star and triangle motifs.
  // If undir is true, then layer configurations are combined as if returning
  // undirected results. Note, that for true undirected results the various
  // directional configurations still need to be summed!
  void Count3MTEdge23Node(double delta, Counter2D& counts, Counter3D& starcounts,
                          Counter3D& trianglecounts, bool undir);

 private:
  // Get all triangles in the static graph, (Us(i), Vs(i), Ws(i)) is the ith
  // triangle.
  void GetAllStaticTriangles(TIntV& Us, TIntV& Vs, TIntV& Ws);
  // Fills nbrs with all neighbors (ignoring direction) of the node in the
  // static graph.
  void GetAllNeighbors(int node, TIntV& nbrs);
  // Fills nodes with a vector of all nodes in the static graph.
  void GetAllNodes(TIntV& nodes);

  // Checks whether or not there is an edge along the static edge (u, v)
  bool HasEdges(int u, int v);

  // A simple wrapper for adding triad edge data
  // dir correspond to the direction with the neighbour node as the reference
  // (0 indexes outgoing and 1 indexes incoming edges to the neighbour).
  // u_or_v is 0 for u and 1 for v.
  void AddMultTriadEdgeData(TVec<TIntTriple>& ts_indices, TVec<MultTriadEdgeData>& events,
                        int& index, int u, int v, int nbr, int dir, int u_or_v);
  // A simple wrapper for adding star edge data
  void AddMultStarEdgeData(TVec<TIntTriple>& ts_indices, TVec<MultStarEdgeData>& events,
           int& index, int u, int v, int nbr, int dir);
  // A simple wrapper for adding edge data
  void AddMultEdgeData(TVec<TIntTriple>& ts_indices, TVec<MultEdgeData>& events,
           int& index, int u, int v, int dir);

  // Directed graph from ignoring timestamps
  PNGraph static_graph_;

  // Core data structure for storing temporal edges.  temporal_data_[u](v) is a
  // list of temporal edges along the static edge (u, v).
  TVec< THash<TInt, TIntV> > temporal_data_;

  // Core data structure for storing layer data
  TVec< THash<TInt, TIntV> > layer_data_;

  // Core data structure for storing edge attribute values
  TVec< THash<TInt, TIntV> > edge_attribute_data_;

  // Indicates the highest layer label, assuming no gaps it also indicates the number of layers
  TInt nrlayers_;
};

//==================================================================================

// This class exhaustively counts all three-edge multilayer temporal edge motifs
class ThreeMTEdgeMotifCounter {
 public:
  // Initialize counter with a given alphabet size
  ThreeMTEdgeMotifCounter(int nrlayers) : nrlayers_(nrlayers) {}

  // Count all three-edge edge motifs with corresponding timestamps.
  void Count(const TVec<TVec<TVec<MultEdgeData> > >& event_string, const TIntV& timestamps,
             double delta, Counter7D& counts);

 private:
  void IncrementCounts(const TVec<TVec<MultEdgeData> >& events);
  void DecrementCounts(const TVec<TVec<MultEdgeData> >& events);
  // One edge counters
  Counter2D conc_nodes_;      // Local use only
  Counter2D tmp_nodes_;       // Local use only
  Counter2D nodes_;
  // Two edge counters
  Counter4D conc_sum_;        // Local use only
  Counter4D tmp_sum_;         // Local use only
  Counter4D pre_conc_sum_;
  Counter4D pre_partial_sum_; // Local use only
  Counter4D tmp_pp_sum_;      // Local use only
  Counter4D sum_;
  // Three edge counters
  Counter6D concurrent_;
  Counter6D pre_partial_;
  Counter6D post_partial_;
  Counter6D serial_;
  int nrlayers_; // number of layers
};

//==================================================================================

// This class exhaustively counts all three-edge multilayer temporal edge motifs
// Counts too many, but exactly enough to be used for deducting triangle counts.
class ThreeMTEdgeMotifCounterToDeduct {
 public:
  // Initialize counter with a given alphabet size
  ThreeMTEdgeMotifCounterToDeduct(int nrlayers) : nrlayers_(nrlayers) {}

  // Count all three-edge edge motifs with corresponding timestamps.
  void Count(const TVec<TVec<TVec<MultEdgeData> > >& event_string, const TIntV& timestamps,
             double delta, Counter7D& counts);

 private:
  void IncrementCounts(const TVec<TVec<MultEdgeData> >& events);
  void DecrementCounts(const TVec<TVec<MultEdgeData> >& events);
  // One edge counters
  Counter2D conc_nodes_;      // Local use only
  Counter2D tmp_nodes_;       // Local use only
  Counter2D conc_pre_nodes_;  // Local use only
  Counter2D nodes_;
  // Two edge counters
  Counter4D conc_sum_;        // Local use only
  Counter4D conc_sum_rev_;    // Local use only
  Counter4D conc_mid_sum_;    // Local use only
  Counter4D tmp_sum_;         // Local use only
  Counter4D pre_conc_sum_;
  Counter4D pre_conc_sum_rev_;
  Counter4D pre_partial_sum_; // Local use only
  Counter4D tmp_pp_sum_;      // Local use only
  Counter4D sum_;
  // Three edge counters
  Counter6D concurrent_;
  Counter6D pre_partial_;
  Counter6D pre_partial_rev_;
  Counter6D post_partial_;
  Counter6D post_partial_rev_;
  Counter6D serial_;
  int nrlayers_; // number of layers
};

//==================================================================================

// Base class for 3-edge, 3-node star and triangle counters.  The template type
// describes the data needed when processing an edge.
template <typename EdgeData>
class StarTriad3MTEdgeCounter {
 public:
  StarTriad3MTEdgeCounter() {}
  void Count(const TVec<TVec<TVec<EdgeData> > >& events, const TIntV& timestamps, double delta);

 protected:
  // These methods depend on the motif type (star or triad).
  virtual void InitializeCounters() = 0;
  virtual void PopPre(const TVec<TVec<EdgeData> >& events) = 0;
  virtual void PopPos(const TVec<TVec<EdgeData> >& events) = 0;
  virtual void PushPre(const TVec<TVec<EdgeData> >& events) = 0;
  virtual void PushPos(const TVec<TVec<EdgeData> >& events) = 0;
  virtual void ProcessCurrent(const TVec<TVec<EdgeData> >& events) = 0;
};

//==================================================================================

// Class for counting star motifs with a given center node.
class ThreeMTEdgeStarCounter : public StarTriad3MTEdgeCounter<MultStarEdgeData> {
 public:
  // Construct class with maximum number of neighbor nodes.  Each processed edge
  // consists of a neighbor and a direction where the neighbor is represented by
  // an int belong to the set {0, 1, ..., max_nodes - 1}.
  ThreeMTEdgeStarCounter(int max_nodes, int nrlayers) : max_nodes_(max_nodes), nrlayers_(nrlayers) {}

  // Counting conventions follow MultTempMotifCounter::Count3MTEdge3NodeStars().
  TInt64 ConcCount(int dir1, int lay1, int dir2, int lay2, int dir3, int lay3) {
    return conc_(dir1, lay1, dir2, lay2, dir3, lay3);
  }
  TInt64 PostPartialCount(int dir1, int lay1, int dir2, int lay2, int dir3, int lay3) {
    return post_partial_(dir1, lay1, dir2, lay2, dir3, lay3);
  }
  TInt64 PostConcCount(int dir1, int lay1, int dir2, int lay2, int dir3, int lay3) {
    return post_conc_(dir1, lay1, dir2, lay2, dir3, lay3);
  }
  TInt64 PostCount(int dir1, int lay1, int dir2, int lay2, int dir3, int lay3) {
    return post_(dir1, lay1, dir2, lay2, dir3, lay3);
  }
  TInt64 PreConcCount(int dir1, int lay1, int dir2, int lay2, int dir3, int lay3) {
    return pre_conc_(dir1, lay1, dir2, lay2, dir3, lay3);
  }
  TInt64 PrePartialCount(int dir1, int lay1, int dir2, int lay2, int dir3, int lay3) {
    return pre_partial_(dir1, lay1, dir2, lay2, dir3, lay3);
  }
  TInt64 MidCount(int dir1, int lay1, int dir2, int lay2, int dir3, int lay3) {
    return mid_(dir1, lay1, dir2, lay2, dir3, lay3);
  }
  TInt64 PreCount(int dir1, int lay1, int dir2, int lay2, int dir3, int lay3) {
    return pre_(dir1, lay1, dir2, lay2, dir3, lay3);
  }

 protected:
  void InitializeCounters();
  void PopPre(const TVec<TVec<MultStarEdgeData> >& events);
  void PopPos(const TVec<TVec<MultStarEdgeData> >& events);
  void PushPre(const TVec<TVec<MultStarEdgeData> >& events);
  void PushPos(const TVec<TVec<MultStarEdgeData> >& events);
  void ProcessCurrent(const TVec<TVec<MultStarEdgeData> >& events);

 private:
  int max_nodes_;
  int nrlayers_;
  // Single edge counters
  Counter3D pre_nodes_;
  Counter3D post_nodes_;
  Counter3D conc_nodes_;        // Local use only
  Counter3D tmp_nodes_;         // Local use only
  Counter3D conc_pre_nodes_;    // Local use only
  // Two edge counters
  Counter4D conc_sum_;          // Local use only
  Counter4D conc_mid_sum_;      // Local use only
  Counter4D post_partial_sum_;  // Local use only
  Counter4D post_conc_sum_;
  Counter4D post_sum_;
  Counter4D pre_conc_sum_;
  Counter4D pre_partial_sum_;   // Local use only
  Counter4D mid_sum_;
  Counter4D pre_sum_;
  Counter4D tmp_sum_;           // Local use only
  Counter4D tmp_post_sum_;      // Local use only
  Counter4D tmp_pre_sum_;       // Local use only
  // Three edge motif counters
  Counter6D conc_;          // M_{3s,1,x}
  Counter6D post_partial_;  // M_{3s,2,x}
  Counter6D post_conc_;     // M_{3s,3,x}
  Counter6D post_;          // M_{3s,4,x}
  Counter6D pre_conc_;      // M_{3s,5,x}
  Counter6D pre_partial_;   // M_{3s,6,x}
  Counter6D mid_;           // M_{3s,7,x}
  Counter6D pre_;           // M_{3s,8,x}
};

//==================================================================================

// Class for counting triangle motifs that contain a specific undirected edge.
class ThreeMTEdgeTriadCounter : public StarTriad3MTEdgeCounter<MultTriadEdgeData> {
 public:
  // Construct class with maximum number of neighbor nodes.  Each processed edge
  // consists of a neighbor, a direction, and an indicator of which end point it
  // connects with.  Each neighbor is represented by an int belong to the set
  // {0, 1, ..., max_nodes - 1}.
  ThreeMTEdgeTriadCounter(int max_nodes, int node_u, int node_v, int nrlayers) :
      max_nodes_(max_nodes), node_u_(node_u), node_v_(node_v), nrlayers_(nrlayers) {}

  // Counting conventions follow MultTempMotifCounter::Count3MTEdgeTriads().
  TInt64 ConcCount(int i, int j, int lay1, int lay2, int lay3) {
    return conc_(i, j, lay1, lay2, lay3);
  }
  TInt64 PrePartialCount(int i, int j, int lay1, int lay2, int lay3) {
    return pre_partial_(i, j, lay1, lay2, lay3);
  }
  TInt64 PostPartialCount(int i, int j, int lay1, int lay2, int lay3) {
    return post_partial_(i, j, lay1, lay2, lay3);
  }
  TInt64 SerialCount(int dir1, int lay1, int dir2, int lay2, int dir3, int lay3) {
    return serial_(dir1, lay1, dir2, lay2, dir3, lay3);
  }

 protected:
  void InitializeCounters();
  void PopPre(const TVec<TVec<MultTriadEdgeData> >& events);
  void PopPos(const TVec<TVec<MultTriadEdgeData> >& events);
  void PushPre(const TVec<TVec<MultTriadEdgeData> >& events);
  void PushPos(const TVec<TVec<MultTriadEdgeData> >& events);
  void ProcessCurrent(const TVec<TVec<MultTriadEdgeData> >& events);
  bool IsEdgeNode(int nbr) { return nbr == node_u_ || nbr == node_v_; }

 private:
  int max_nodes_;
  // Single edge counters
  Counter4D pre_nodes_;
  Counter4D pos_nodes_;
  Counter4D conc_nodes_;        // Local use only
  Counter4D tmp_nodes_;         // Local use only
  Counter4D conc_pre_nodes_;    // Local use only
  // Two edge counters
  Counter5D pre_sum_;
  Counter5D post_sum_;
  Counter5D mid_sum_;
  Counter5D conc_sum_;          // Local use only
  Counter5D conc_mid_sum_;      // Local use only
  Counter5D pre_conc_sum_;
  Counter5D post_conc_sum_;
  Counter5D pre_partial_sum_;   // Local use only
  Counter5D post_partial_sum_;  // Local use only
  Counter5D tmp_sum_;           // Local use only
  Counter5D tmp_pre_sum_;       // Local use only
  Counter5D tmp_post_sum_;      // Local use only
  // Three edge motif counters
  Counter5D conc_;          // M_{3t,1,x}
  Counter5D pre_partial_;   // M_{3t,2,x}
  Counter5D post_partial_;  // M_{3t,3,x}
  Counter6D serial_;        // M_{3t,4,x}
  // Two end points of the edge whose triangles this class counts.
  int node_u_;
  int node_v_;
  int nrlayers_;
};

#endif  // snap_temporalmotifs_h
